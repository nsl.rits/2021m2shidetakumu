#define NS_LOG_APPEND_CONTEXT                                   \
  if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#include "shide-normal-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>
#include <unistd.h>
#include <map>
#include <time.h> //time()
#include <stdlib.h> //rand()

#define Nnumber 44 //全ノード数(RSU+車両) 44 or 34 or 24
#define PROBABILITY 100 //rebraodcast %
#define MOUNTAIN_RSU_RANGE 22500  //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 19000 22500 でいい = 150m 
#define NEGATIVE_MOUNTAIN_RSU_RANGE -22500
#define HYOUKA_MOUNTAIN_RSU_RANGE 22500 //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 22500 = 150m 
#define HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE -22500
#define HYOUKA 1 //1 = 対向車両と総パケット数, 2 = 後方車両
#define HYOKA_KUKAN 400 //左発の車両がどのx座標を通過したら評価を開始するか 100 = 左端, 200 = 左端と山頂の間, 300 = 山頂, 400 = 右端と山頂の間
#define HYOUKA_END_DISTANCE 0 // 評価をどこまで取るか（左発と右発の先頭車両の距離で判別）0 = 0m,10000 =100m

#define NOMAL_NODE_RANGE 10000 //通常RSUの通信範囲
#define NEGATIVE_NOMAL_NODE_RANGE -10000 //通常RSUの通信範囲
#define UPHILL_RANGE 4444 //通常RSUの上り通信範囲
#define NEGATIVE_UPHILL_RANGE -4444 //通常RSUの上り通信範囲

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ShideNormalRoutingProtocol");

namespace shideNormal {
NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);

int hoptemp[4000][Nnumber+1] = {0}; //ホップ数記録用
int packettemp[4000][Nnumber+1] = {0}; //受信パケットリスト
int packettemp_t[4000][Nnumber+1] = {0}; //時間遅延のための変数
int sendn_rist[1000]={0};
int recv_rist[200][200] = {0};
int8_t hop = 0; //ホップ数
int pnm = 0; //パケットシーケンスナンバー
int8_t dire = 0; //進行方向
int rh = 1; //進行方向と逆パケットのホップ数
int otemp=0; //パケット生成者
int packetcnt=0; //パケット受信数カウンタ
int rs = Nnumber-1; //パケット送信ノード番号
int sendn = Nnumber-1; //パケット送信ノード番号

int sendn_success_list[10000] = {0}; //確率ブロードキャストを成功したノードのリスト
int success_count = 0; //確率ブロードキャストを成功した数（番号）
int success_node_bcst_count = 0; //Rbroadcastで使用するブロードキャストを成功したノードを指定する番号

int max_x=0;
int max_y=0;
int min_x=0;
int min_y=0;



int random_i=0; //random counter
std::map<int,int> m1; //x座標
std::map<int,int> m2; //y座標
std::map<int,int> vd_rist; //車両の A or B の記録リスト

int mountain_rsu[100] = {0}; //(mountain_rsu[n] = 1)なら山頂RSU
int RSU_center_number; //真ん中のRSU

//評価用変数
Time vehicle_send_time; //パケット送信時刻 
double receive_count[100] = {0.0}; //パケット到達率の分子
double evaluation_send_count = 1.0; //パケット到達率の分母
double packet_totatu[100] = {0.0}; //パケット到達率
Time average_delay_time[100]; //平均遅延時間
Time total_delay_time[100]; //遅延時間の合計
int evaluation_start = 0; //評価スタートフラグ
int hoptest[Nnumber]={0};//パケット受信時のホップカウントを記録する
int hop_c=0; //sendn_rist2[hop_c][n]　ホップ数毎の受信ノードID
int sendn_rist2[1000][1000]={100}; //[ホップ数][受け取ったノードID]
int send_zyun_node[100]={100}; //送信ノードIDを送信順に記録する
int send_zyun_node_count = 0; //送信ノードIDを送信順のカウント
int send_end_count = 0; //送信完了回数（ホップ数）
double total_hop=0.0; //send_end_count no goukei
double heikin_receive_hop =0.0; //1回受信当たりの受信時ホップ数

/// UDP Port for SHIDE control traffic
const uint32_t RoutingProtocol::SHIDE_PORT = 654;


RoutingProtocol::RoutingProtocol ()
{
}

TypeId
RoutingProtocol::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::shideNormal::RoutingProtocol")
    .SetParent<Ipv4RoutingProtocol> ()
    .SetGroupName ("ShideNormal")
    .AddConstructor<RoutingProtocol> ()
    .AddAttribute ("UniformRv",
                   "Access to the underlying UniformRandomVariable",
                   StringValue ("ns3::UniformRandomVariable"),
                   MakePointerAccessor (&RoutingProtocol::m_uniformRandomVariable),
                   MakePointerChecker<UniformRandomVariable> ());
  return tid;
}

RoutingProtocol::~RoutingProtocol ()
{
}

//DoDispose=処理する
void
RoutingProtocol::DoDispose ()
{
}

//ルーティングテーブルエントリを出力する。
void
RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
{
  *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
                        << "; Time: " << Now ().As (unit)
                        << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
                        << ", SHIDE Routing table" << std::endl;
  //Print routing table here.
  *stream->GetStream () << std::endl;
}

int64_t
RoutingProtocol::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_uniformRandomVariable->SetStream (stream);
  return 1;
}

Ptr<Ipv4Route>
RoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                              Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
{
  std::cout<<"Route Ouput Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  Ptr<Ipv4Route> route;

  if (!p)
    {
	  std::cout << "loopback occured! in routeoutput";
	  return route;// LoopbackRoute (header,oif);
	}

  if (m_socketAddresses.empty ())
    {
	  sockerr = Socket::ERROR_NOROUTETOHOST;
	  NS_LOG_LOGIC ("No zeal interfaces");
	  std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";

	  Ptr<Ipv4Route> route;
	  return route;
    }  
  return route;
}

bool
RoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                             Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                             MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
{
 
  std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  return true;
}

//IPv4アドレスのセット
void
RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
{
  NS_ASSERT (ipv4 != 0);
  NS_ASSERT (m_ipv4 == 0);
  m_ipv4 = ipv4;
}

void
RoutingProtocol::NotifyInterfaceUp (uint32_t i)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                        << " interface is up");
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
  if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
    {
      return;
    }
  // Create a socket to listen only on this interface
  Ptr<Socket> socket;

  socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvShide,this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetLocal (), SHIDE_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketAddresses.insert (std::make_pair (socket,iface));

    // create also a subnet broadcast socket
  socket = Socket::CreateSocket (GetObject<Node> (),
                                 UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvShide, this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetBroadcast (), SHIDE_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));

  if (m_mainAddress == Ipv4Address ())
    {
      m_mainAddress = iface.GetLocal ();
    }

  NS_ASSERT (m_mainAddress != Ipv4Address ());
}

void
RoutingProtocol::NotifyInterfaceDown (uint32_t i)
{
}

void
RoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
{
}

void
RoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
{
}

//送信車両の位置を取得
void
RoutingProtocol::GetSourcePosition () 
{
  int xpos,ypos;
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
 if(id == Nnumber-1){
  Ptr<WaypointMobilityModel> mobility = m_ipv4->GetObject<WaypointMobilityModel>();  
  xpos = mobility->GetPosition().x;
  ypos = mobility->GetPosition().y;
  m1[id] = xpos; //map(m1)にx座標を格納
  m2[id] = ypos; //map(m2)にy座標を格納
 }
 if(id == Nnumber){
  Ptr<WaypointMobilityModel> mobility = m_ipv4->GetObject<WaypointMobilityModel>();  
  xpos = mobility->GetPosition().x;
  ypos = mobility->GetPosition().y;
  m1[id] = xpos; //map(m1)にx座標を格納
  m2[id] = ypos; //map(m2)にy座標を格納
  }
}

//後方車両の位置を取得(ソースノードでもあるnode id == Nnumber の位置)
void
RoutingProtocol::GetDestPosition () 
{
  int xpos,ypos;
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
//for(id = Nnumber-9; id <= Nnumber-2; id++){
 if(Nnumber-9 <= id && id <= Nnumber-2){
  Ptr<WaypointMobilityModel> mobility = m_ipv4->GetObject<WaypointMobilityModel>();  
  xpos = mobility->GetPosition().x;
  ypos = mobility->GetPosition().y;
  m1[id] = xpos; //map(m1)にx座標を格納
  m2[id] = ypos; //map(m2)にy座標を格納
  }
//}
}



//rsuの位置を取得
void
RoutingProtocol::GetRsuPosition () 
{
  int xpos,ypos;
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
  if(id < Nnumber-9)
  {
        Ptr<ConstantPositionMobilityModel> mobility = m_ipv4->GetObject<ConstantPositionMobilityModel>();  
        xpos = mobility->GetPosition().x;
        ypos = mobility->GetPosition().y;
        m1[id] = xpos; //map(m1)にx座標を格納
        m2[id] = ypos; //map(m2)にy座標を格納
  /////  mapにそれぞれ座標が入っているか出力（確認用）  /////
  /*
  std::map<int,int>::iterator it;
  it = m1.find(id);
  std::cout << "nodeID" << it->first << " => " << "x座標" << it->second << std::endl;
  it = m2.find(id);
  std::cout << "nodeID" << it->first << " => " << "y座標" << it->second << std::endl;
  */
  }
}

int new_x=0;
int old_x=0;
int new_y=0;
int old_y=0;

int new2_x=0;
int old2_x=0;
int new2_y=0;
int old2_y=0;


/////////*        sourceノードのx,yのベクトルを取得        *////////////
void 
RoutingProtocol::NewSourceDirection () 
{
  WindingHeader windingHeader;
  int s_vector_x,s_vector_y; //ルートをしてないベクトル
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();  
  std::map<int,int>::iterator it;

  if(id == Nnumber-1) // sourceノードの時
  {
    //x
    it = m1.find(Nnumber-1);
    old_x = new_x;
    new_x = it->second;
    s_vector_x = (new_x - old_x)*(new_x - old_x); 
    //printf("new_x:%d old_x:%d vec_x:%d\n",new_x,old_x,s_vector_x);
    windingHeader.SetOriginPositionVectorX (s_vector_x);//パケットにベクトルxをセット

    /*test
    int test;
    test=windingHeader.GetOriginPositionVectorX ();
    printf("test:%d\n",test);
    */

    //y
    it = m2.find(Nnumber-1);
    old_y = new_y;
    new_y = it->second;
    s_vector_y = (new_y - old_y)*(new_y - old_y);
    windingHeader.SetOriginPositionVectorY (s_vector_y);//パケットにベクトルyをセット
  }

  if(id == Nnumber) // sourceノードの時
  {
    //x
    it = m1.find(Nnumber);
    old2_x = new2_x;
    new2_x = it->second;
    s_vector_x = (new2_x - old2_x)*(new2_x - old2_x); 
    windingHeader.SetOriginPositionVectorX (s_vector_x);//パケットにベクトルxをセット

    /*test
    int test;
    test=windingHeader.GetOriginPositionVectorX ();
    printf("test:%d\n",test);
    */

    //y
    it = m2.find(Nnumber);
    old2_y = new_y;
    new2_y = it->second;
    s_vector_y = (new2_y - old2_y)*(new2_y - old2_y);
    windingHeader.SetOriginPositionVectorY (s_vector_y);//パケットにベクトルyをセット
  }
}


void
RoutingProtocol::Get_mountain_rsu_list ()  //山頂RSUリストの作成
{
  if(Nnumber == 24)
  {
    mountain_rsu[7] = 1;
  }
  if(Nnumber == 34)
  {
    mountain_rsu[12] = 1;
  }
  if(Nnumber == 44)
  {
    mountain_rsu[7] = 1;
  }
  //printf("check : %d ", mountain_rsu[7]); //check
}

void
RoutingProtocol::DoInitialize (void)
{
  int i=0;
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();

  GetRsuPosition();
  Get_mountain_rsu_list(); //山頂RSUリストの作成

  WindingHeader windingHeader;

  if(id == Nnumber-1)   //if Node ID = Nnumber-1, center is 7 or 12 or 17
  {
    for(i=0;i<=700;i++)
    {
      Simulator::Schedule(Seconds(i* 0.2+1), &RoutingProtocol::SendXBroadcast, this);
      Simulator::Schedule(Seconds(i* 0.2+1), &RoutingProtocol::GetSourcePosition, this); //送信時の位置を取得する
      //Simulator::Schedule(Seconds(i* 0.2+1), &RoutingProtocol::GetDestPosition, this);
      Simulator::Schedule(Seconds(i* 1), &RoutingProtocol::NewSourceDirection, this); //過去の送信時の位置と比較（ベクトル）しA,Bを出す
    }
  }

  if(id == Nnumber)   //if Node ID = Nnumber
  {
    for(i=0;i<=700;i++)
    {
      Simulator::Schedule(Seconds(i* 0.2+1.05), &RoutingProtocol::SendXBroadcast, this);
      Simulator::Schedule(Seconds(i* 0.2+1.05), &RoutingProtocol::GetSourcePosition, this); //送信時の位置を取得する
      //Simulator::Schedule(Seconds(i* 0.2+1), &RoutingProtocol::GetDestPosition, this);
      Simulator::Schedule(Seconds(i* 1 +0.05), &RoutingProtocol::NewSourceDirection, this); //過去の送信時の位置と比較（ベクトル）しA,Bを出す
    }
  }
}

void
RoutingProtocol::RecvShide (Ptr<Socket> socket)
{
  //std::cout<<"In recv Shide(SRP)(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  //RecvPureBroadcast(); //pure or probability
  //RecvWindingBroadcast(); //winding1
  //RecvWinding2Broadcast(); //winding2
  RecvWinding3Broadcast(); //winding3
}


/********************* SendXBroadcast 車両のパケット送信 ****************************************/

int a=0;
void
RoutingProtocol::SendXBroadcast (void)
{
  sendn = Nnumber-1; //sender tempの初期化
  hop = 0; //ホップ数の初期化
  rh = 1; //リバースホップ数の初期化
  rs = Nnumber-1;
  hop_c = 0;
  int i=0;
  int j=0;
 ////   center rsu number   //////
  
  if(Nnumber == 44)
  {
    RSU_center_number = 17;
  }
  if(Nnumber == 34)
  {
    RSU_center_number = 12;
  }
    if(Nnumber == 24)
  {
    RSU_center_number = 7;
  }

//// 初期化 /////
  for(i=0;i<4000;i++)
  {
    for(j=0;j<=Nnumber+1;j++)
    {
      hoptemp[i][j] = 0;
    }
  }

  for(i=0;i<4000;i++)
  {
    for(j=0;j<=Nnumber+1;j++)
    {
      packettemp[i][j] = 0;
    }
  }

  for(i=0;i<4000;i++)
  {
    for(j=0;j<=Nnumber+1;j++)
    {
      packettemp_t[i][j] = 0;
    }
  }

  for(i=0;i<1000;i++)
  {
    sendn_rist[i] = 100;
  }

  for(i=0;i<100;i++)
  {
    send_zyun_node[i] = 100;
  }

 for(i=0;i<1000;i++)
  {
    for(j=0;j<=1000;j++)
    {
      sendn_rist2[i][j] = 100;
    }
  }

  for(i=0;i<=100;i++)
  {
    for(j=0;j<=100;j++)
    {
      recv_rist[i][j] = 0;
    }
  }

  for(i=0;i<=Nnumber;i++)
  {
    hoptest[i] = 0;
  }

  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
  {
    Ptr<Socket> socket = j->first;
    Ipv4InterfaceAddress iface = j->second;
    Ptr<Packet> packet = Create<Packet> ();

    WindingHeader windingHeader;
    windingHeader.SetPacketNumber (pnm);
    windingHeader.SetDirection (0);
    windingHeader.SetHopCount (hop);

    if(a == 0)
    {
      sendn = Nnumber -1;
      rs = Nnumber-1;
      sendn_rist[Nnumber-1] = Nnumber-1;
      otemp = sendn;
      std::map<int,int>::iterator it;
      it = m1.find(Nnumber - 1); //X
      windingHeader.SetOriginPositionX (it->second);
      it = m2.find(Nnumber - 1); //Y
      windingHeader.SetOriginPositionY (it->second);
      windingHeader.SetSenderNumber(sendn);
      a=1;      
    }
    else if(a == 1)
    {
      sendn = Nnumber;
      rs = Nnumber;
      sendn_rist[Nnumber] = Nnumber;
      otemp = sendn;
      std::map<int,int>::iterator it;
      it = m1.find(Nnumber); //X
      windingHeader.SetOriginPositionX (it->second);
      it = m2.find(Nnumber); //Y
      windingHeader.SetOriginPositionY (it->second);
      windingHeader.SetSenderNumber(sendn);
      a=0;      
    }
    sendn_rist2[0][sendn] = sendn;
    send_zyun_node_count = 1;
    send_zyun_node[send_zyun_node_count] = sendn;
    send_end_count = 1;
    //send_kanryousita_zyun_node = send_zyun_node[send_end_count];

    windingHeader.SetOrigin (Ipv4Address("10.1.1.13"));
    //windingHeader.SetSenderNumber(Nnumber-1);
    windingHeader.SetLifeTime (Seconds(3));
    hop = windingHeader.GetHopCount () + 1;
    pnm = windingHeader.GetPacketNumber () + 1;
    packet->AddHeader (windingHeader);
      
    TypeHeader tHeader (SHIDETYPE_WINDING);
    packet->AddHeader (tHeader);   

    Ipv4Address destination;
    if(iface.GetMask () == Ipv4Mask::GetOnes ())
    {
      destination = Ipv4Address ("255.255.255.255");
    }
    else
    {
      destination = iface.GetBroadcast ();
    }
    socket->SendTo (packet, 0, InetSocketAddress (destination, SHIDE_PORT));
    //std::cout<<"XXXXXXXXXXXXXXXXXXXXXbroadcast sent(SRP)\n";


    if(otemp==Nnumber-1)
    {
      //std::cout<<"\n"<<Simulator::Now ();
      vehicle_send_time = Simulator::Now();
      std::cout<<"  "<<packetcnt<<"\n"<<Simulator::Now (); //総パケット出力
      if(evaluation_start==1)
      {
        evaluation_send_count++;
        std::cout<<"  " << evaluation_send_count;
      }
    }
  }
}

/********************* SendRBroadcast pure用ブロードキャスト ****************************************/
void
RoutingProtocol::SendRBroadcast (void)
{
  //sendn = sendn_success_list[success_node_bcst_count]; 
  //success_node_bcst_count++;
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
  {
    Ptr<Socket> socket = j->first;
    Ipv4InterfaceAddress iface = j->second;
    Ptr<Packet> packet = Create<Packet> ();

    WindingHeader windingHeader;
    windingHeader.SetPacketNumber (pnm);
    windingHeader.SetDirection (dire);
    windingHeader.SetHopCount (hop);
    //std::map<int,int>::iterator it;
    //it = m1.find(Nnumber-1); //X
    //windingHeader.SetOriginPositionX (it->second);
    //it = m2.find(Nnumber-1); //Y
    //windingHeader.SetOriginPositionY (it->second);
    windingHeader.SetOrigin (Ipv4Address("10.1.1.13"));
    //windingHeader.SetSenderNumber(Nnumber-1);
    windingHeader.SetLifeTime (Seconds(3));
    hop = windingHeader.GetHopCount () + 1;

    packet->AddHeader (windingHeader);
    TypeHeader tHeader (SHIDETYPE_WINDING);
    packet->AddHeader (tHeader);  

    send_end_count = send_end_count+1; //送信完了回数（ホップ数）

    // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
    Ipv4Address destination;
    if (iface.GetMask () == Ipv4Mask::GetOnes ())
    {
      destination = Ipv4Address ("255.255.255.255");
    }
    else
    {
      destination = iface.GetBroadcast ();
    }
    socket->SendTo (packet, 0, InetSocketAddress (destination, SHIDE_PORT));
    //std::cout<<"send time = "<< Simulator::Now() <<"\n"; 
    //std::cout<<"RRRRRRbroadcast sent(SRP)\n"; 


  }
}

/********************* SendWBroadcast winding1,2用ブロードキャスト ****************************************/
void
RoutingProtocol::SendWBroadcast (void)
{
  WindingHeader windingHeader;
 for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
 {
    Ptr<Socket> socket = j->first;
    Ipv4InterfaceAddress iface = j->second;
    Ptr<Packet> packet = Create<Packet> ();

    std::map<int,int>::iterator it;

    windingHeader.SetPacketNumber (pnm);
    windingHeader.SetDirection (dire);
    windingHeader.SetHopCount (hop);
    it = m1.find(Nnumber-1); //X
    windingHeader.SetOriginPositionX (it->second);
    it = m2.find(Nnumber-1); //Y
    windingHeader.SetOriginPositionY (it->second);
    windingHeader.SetOriginPositionVectorX (0); //xベクトル
    windingHeader.SetOriginPositionVectorY (0); //yベクトル
    windingHeader.SetOrigin (Ipv4Address("10.1.1.13"));
    windingHeader.SetSenderNumber(sendn);
    windingHeader.SetLifeTime (Seconds(3));

    packet->AddHeader (windingHeader);
    TypeHeader tHeader (SHIDETYPE_WINDING);
    packet->AddHeader (tHeader);   

    send_end_count = send_end_count+1; //送信完了回数（ホップ数）
    //send_kanryousita_zyun_node = send_zyun_node[send_end_count]; 

    Ipv4Address destination;
    if (iface.GetMask () == Ipv4Mask::GetOnes ())
    {
      destination = Ipv4Address ("255.255.255.255");
    }
    else
    {
      destination = iface.GetBroadcast ();
    }
    socket->SendTo (packet, 0, InetSocketAddress (destination, SHIDE_PORT));

    //std::cout<<"WWWWWWWbroadcast sent(SRP)\n"; 
    //sendn = windingHeader.GetSenderNumber ();
    /*
    if(otemp==Nnumber)
    {
      //printf(" %d",sendn);
      printf("(%d)",rs);
      printf("rh%d",rh);
    }
    */
  }
}


void
RoutingProtocol::RecvTime()
{
}

int GetRamdom(int random_min, int random_max)
{
  return random_min + (int)(rand() * (random_max - random_min + 1.0) / (1.0 + RAND_MAX));
};

/********************* pure flooding or probability****************************************/
void
RoutingProtocol::RecvPureBroadcast()
{
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
  int8_t i = 0;
  std::map<int,int>::iterator it;
  WindingHeader windingHeader;
  int n;
//////////////pure, probability
  int ramdom_index;
  //int sendposy; //送信元の座標
  int sendposx,sendposy; //送信元の座標
  int nextrsuposx,nextrsuposy; //先のRSU座標

  //int myposy,myposx;
  int a;
  //int b; //通信範囲比較用

/*
  for(i = 0; i <= Nnumber-10; i++) //-10はvehicleのリブロードキャストを防ぐため
  {
    if(id == i) //if Node ID = i
    {	
      for(n=0; n<=50; n++)
      {
		    if(1 != packettemp[pnm][i] && sendn_rist[n] != 100)
		    {
          sendn = id;
				  sendn_rist[id] = id;
          Time now_time = Simulator::Now();
          Time wait_time = Time(1111.0);
          //std::cout<<"now + wait = "<< now_time + wait_time<<"\n"; 
          ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
          if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
          {            
          Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
          }
          packettemp[pnm][i] = 1;
          printf(" %d ",id); //id is number of rebroadcast RSU ID
		    }
      }
    }
  }
*/

int center_flag=0;
int left_flag=0;
int right_flag=0;
//int k=0;

  if(id == RSU_center_number)
  {
    if(1 != packettemp[pnm][id])
    {
      for(n=0; n<=Nnumber; n++)
      {
        if(n == Nnumber)
        {
          center_flag = 1;
        }        
        if(sendn_rist2[hop_c][n] != 100)
        {
          sendn=sendn_rist2[hop_c][n];
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元との距離が66m以内だと受信成功とみなす→bcst
          //printf(" fr:(%d,%d)to:(%d,%d),a:%d ",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE  && 1 != packettemp[pnm][id])
          {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1;
          }
        }
      }
    }
  }

//受信ノードが左側である
  if(id < RSU_center_number)
  {
    if(1 != packettemp[pnm][id])
    {
      for(n=0; n<=Nnumber; n++)
      {
        if(n == Nnumber)
        {
          left_flag = 1;
        }
        if(sendn_rist2[hop_c][n] != 100)
        {
          sendn = sendn_rist2[hop_c][n];
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx); 
          //printf(" fr:(%d,%d)to:(%d,%d),a:%d ",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
  //①送信元が山頂RSUである
          if(sendn == RSU_center_number)
          {
    //山頂RSUの通信範囲内なら受信成功→bcst
            if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE && 1 != packettemp[pnm][id] )
            {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;           
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1;
            }
           }
  //②送信元が受信ノードより右側　かつ　送信ノードが真ん中より左側のノードである
            else if(nextrsuposx < sendposx && sendposx < 300)
            {
    //100m以内だと通信成功→bcst
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp[pnm][id])
              {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1;       
              }
            }
  //①②以外の場合
            else
           {
  //66m以内だと通信成功→bcst
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp[pnm][id])
            {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1; 
            }
          }
        }
      }
    }
  }

  

//受信ノードが右側である
  if(RSU_center_number < id && id <= Nnumber-10)
  {
    if(1 != packettemp[pnm][id])
    {
      for(n=0; n<=Nnumber; n++)
      {
        if(n == Nnumber)
        {
          right_flag = 1;
        }
        if(sendn_rist2[hop_c][n] != 100)
        {
          sendn = sendn_rist2[hop_c][n];
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx); 
          //printf(" fr:(%d,%d)to:(%d,%d),a:%d ",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
  //①送信元が山頂RSUである
          if(sendn == RSU_center_number)
          {  
    //山頂RSUの通信範囲内なら受信成功→bcst
            if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE && 1 != packettemp[pnm][id])
            {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1;
            }
          }    
  //②送信元が受信ノードより左側　かつ　送信ノードが真ん中より右側のノードである
          else if(sendposx < nextrsuposx && 300 < sendposx)
          {
    //100m以内だと通信成功→bcst
            if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp[pnm][id])
            {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1;          
            }
          }
  //①②以外の場合
          else
          {
  //66m以内だと通信成功→bcst
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp[pnm][id])
            {
            Time now_time = Simulator::Now();
            Time wait_time = Time(1111.0);
            ramdom_index = GetRamdom(1,100); //１〜１００の数字をランダムで取得
            if(ramdom_index <= PROBABILITY) //ブロードキャストを行う確率 <= "%"
            {            
            hoptest[id] = hoptest[sendn]+1;
            sendn = id;
            sendn_rist[id] = id;
            sendn_rist2[hop_c][id] = id;
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Simulator::Schedule(wait_time, &RoutingProtocol::SendRBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            }
            //printf(" %d",sendn);
            packettemp[pnm][id] = 1;
            }
          }
        }
      }
    }
  }
if(center_flag == 1 && left_flag ==1 && right_flag ==1)
{
  hop_c = hop_c + 1;
  center_flag = 0;
  left_flag = 0;
  right_flag  = 0;
}

/****************************     対向車遅延時間(pure left→right)     *****************************/
if(HYOUKA ==1)
{
  for(i=0;i<=send_end_count;i++)
  {
    if(send_zyun_node[i] != 100)
    {
      if(id == Nnumber) //destination node
      {
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber-1); //source車両 x,y
                        int sendposx = it->second;
                        it = m2.find(Nnumber-1);
                        int sendposy = it->second;
                        it = m1.find(id); //destination x,y
                        int nextrsuposx = it->second;
                        it = m2.find(id);
                        int nextrsuposy = it->second;
                        int b = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx); //source car no tuusinhanni ni hairumade
                        if(nextrsuposx > sendposx && 500 > sendposx && sendposx >= HYOKA_KUKAN && b >= HYOUKA_END_DISTANCE && otemp == Nnumber-1 ) //source車両とdestinationがすれ違うまで
                        //if(100 < nextrsuposx && nextrsuposx < sendposx && otemp == Nnumber-1 && b >=10000) //source車両とdestinationがすれ違うまで
                        {

                                it = m1.find(send_zyun_node[i]); //sender x,y
                                sendposx = it->second;
                                it = m2.find(send_zyun_node[i]);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                    if(send_zyun_node[i]==RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
                                    {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time one_delay_time = receive_time - vehicle_send_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id] << " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                    }
                                   if(send_zyun_node[i] != RSU_center_number && nextrsuposx <= sendposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time one_delay_time = receive_time - vehicle_send_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                   }
                                   if(send_zyun_node[i] != RSU_center_number &&  nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time one_delay_time = receive_time - vehicle_send_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id];
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
//printf("%d",sendn_rist[j]);
                                   }

                        }
                }  
      }
    }
  }
}
/****************************     対向車遅延時間(pure right→left)     *****************************/
/*
  for(i=0;i<=Nnumber;i++)
  {
    if(sendn_rist[i] != 100)
    {
      if(id == Nnumber-1) //destination node
      {
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber); //destination  x
                        int sendposx = it->second;
                        it = m1.find(Nnumber-1); //source x
                        int nextrsuposx = it->second;
                        if(nextrsuposx > sendposx && otemp == Nnumber ) //source車両とdestinationがすれ違うまで
                        {
                                        std::cout<<"　"<< Simulator::Now ();
                                        packettemp[pnm][id] = 1;
                                        printf(" %d ",id);
                             
                        }
                }  
      }
    }
  }
*/
/********************************************** パケット受信数のカウント ***************************************************/
/*
 if(0<=id && id<=Nnumber)
  {
    GetSourcePosition();
    it = m1.find(Nnumber-1); //right車両 x
    int sendposx = it->second;
    if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
    {
      packetcnt++;
    }
  }
*/
//車両ノードである

  for(i = Nnumber-9; i <= Nnumber; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
        for(n=0; n<=50; n++)
        {
          if(sendn_rist[n] != 100)
          {
  //受け取ったパケットの送信元リストにのっていない場合（車両のパケット送信毎にリストを初期化している）
            if(recv_rist[id][sendn_rist[n]] != 1)
            {
              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元ノードが山頂RSUの場合はそのままの通信範囲で受信成功とみなす
    	        if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //受信車両の位置が真ん中より右側の場合
              else if(300 < nextrsuposx)
              {
    //送信元が自分より左側で真ん中より右側の場合は距離が100m以内だと通信成功とみなす
                if(300 < sendposx && sendposx < nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
  //受信車両の位置が真ん中より左側の場合
              else if(300 > nextrsuposx)
              {
    //送信元が自分より右側で真ん中より左側の場合は距離が100m以内だと通信成功とみなす
                if(300 > sendposx && sendposx > nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
            }
          }
        }
      }
    }
  }



//左側のRSUノードである
  for(i = 0; i <= RSU_center_number-1; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より左側の場合はお互いの距離が６６m以内だと受信成功とみなす
              else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードかつ、自分より右側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx < 300 && nextrsuposx < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
      }
    }
  }

//真ん中のRSUノードである
  if(id == RSU_center_number)
  {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
    for(n=0; n<=50; n++)
    {
		  if(sendn_rist[n] != 100)
		  {
      if(recv_rist[id][sendn_rist[n]] != 1)
{        
  //送信元の全ノードが66m以内だと受信成功とみなす→bcst
        it = m1.find(sendn_rist[n]); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn_rist[n]);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
        {
          packetcnt++;
          recv_rist[id][sendn_rist[n]] = 1;
        }
      }
    }
    }
  }
  }

//右側のRSUノードである
  for(i = RSU_center_number+1; i <= Nnumber-10; i++)
  {
    if(id == i )
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より右側の場合はお互いの距離が６６m以内だと受信成功とみなす→bcst
              else if(sendposx > nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードかつ、自分より左側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx > 300 && nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
    }
    }
  }


/**************************************leftから後方車両へ(pure)  *****************************************/
if(HYOUKA == 2)
{
  int j;
  for(j=0;j<=send_end_count;j++)
  {
    if(send_zyun_node[j] != 100)
    {
      if(Nnumber-9 <= id && id <= Nnumber-6)
      {
        if(1 != packettemp_t[pnm][id])
        {
          GetDestPosition();
          it = m1.find(Nnumber-1); //source車両 x,y
          sendposx = it->second;
          it = m2.find(Nnumber-1);
          sendposy = it->second;
          if(500 > sendposx && sendposx >= 100 && otemp == Nnumber-1) //source車両が峠道区間をぬけるまで
          {
            it = m1.find(send_zyun_node[j]); //source車両 x,y
            sendposx = it->second;
            it = m2.find(send_zyun_node[j]);
            sendposy = it->second;
            it = m1.find(id); //my  x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
//送信ノードが真ん中にいる（送信車両が山頂RSUノード）
            if(send_zyun_node[j] == RSU_center_number)
            {
  //山頂RSUの通信距離内なら受信成功
              if(HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
//                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
//送信ノードが左側にいる（送信車両も含む）
            else if(sendposx < 300)
            {
  //送信ノードより受信ノードの方が左側の場合
              if(nextrsuposx < sendposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id])
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
//                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                  packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
//                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              } 
            }
//送信ノードが右にいる（送信車両も含む）
            else if(300 < sendposx)
            {
  //送信ノードより受信ノードの方が右側の場合
              if(sendposx < nextrsuposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id])
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
//                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                  packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功     
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
//                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
          }
        }
      }
    }
  }
}
/**************************************right車両から後方車両へ(pure)  *****************************************/
/*
  for(i=0;i<=Nnumber;i++)
  {
    if(sendn_rist[i] != 100)
    {
      if(Nnumber-5 <= id && id <= Nnumber-2) //destination node
      {
                if(1 != packettemp_t[pnm][id])
                {
                        GetSourcePosition();
                        GetDestPosition();
                        it = m1.find(Nnumber); //source車両 x,y
                        int sendposx = it->second;
                        if(500 > sendposx && sendposx >= 100 && otemp == Nnumber) //source車両 ga hasi np rsu ni toutatu surumade
                        {
                                        std::cout<<"　"<< Simulator::Now ();
                                        //printf("recv packet!!\n");
                                        packettemp_t[pnm][id] = 1;
                        }
                }  
      }      
    }
  }

  */
} //pure flooding

/*************************** Winding flooding **********************************/
void
RoutingProtocol::RecvWindingBroadcast()
{
  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
  int i = 0;
  int sendposx,sendposy;
  int nextrsuposx,nextrsuposy;
  //int myposy,myposx;
  int a;
  int n;
  WindingHeader windingHeader;
  std::map<int,int>::iterator it;

//右半分のRSU（山頂,両端ノードを除く）
  for(i = RSU_center_number+1; i <= Nnumber-11; i++)
  {
    if(id == i) //if Node ID = i
    {
      if(1 != packettemp[pnm][i])
      {
/*            if(sendn == RSU_center_number)
            {
              printf(" %d ",id);
            }        */
        hoptemp[pnm][i] = windingHeader.GetHopCount (); //格納忘れ防止
        it = vd_rist.find(otemp);        
  //発信元の車両がA方向の時(右側出発の送信車両)
        if(0 == it->second)
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //送信元が自分より右側の位置の時で66m以内で受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx )
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
      //左方向側の１つ先のRSUが66m以内で受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                 //何もしない
              }
      //それ以外
              else
              {
        //bcst  if（rh+1）
                windingHeader.SetSenderNumber (i);
                sendn = i;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = i;
                  rh = rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][i] = windingHeader.GetHopCount ();
                packettemp[pnm][i] = 1;
              } 
            }
    //自分より左側にいる送信者から受信したとみなした場合
            else if((NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx < nextrsuposx && sendn != RSU_center_number)
             || (NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE && sendposx < nextrsuposx && sendn == RSU_center_number))
            {
      //送信元が右側出発車両で自分より左側にいる場合
              if(sendn == Nnumber-1 || sendn ==Nnumber)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ左側のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
        //それ以外
                else
                {
          //bcst　if（rh+1）
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop == 1)
                  {
                    rs = i;
                    rh=rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //送信元がRSUで自分より左側にいる場合かつ"rh<=2"の場合
              else if(sendn < Nnumber-9 && rh==2 && hop == 2)
              {
        //送信元が山頂RSUの場合
                if(sendn == RSU_center_number)
                {
                  it = m1.find(sendn); //sender x,y
                  sendposx = it->second;
                  it = m2.find(sendn);
                  sendposy = it->second;
                  it = m1.find(id+1); //next A RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id+1);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          //1つ右側先のRSUが山頂通信距離以内で受信していたとみなした場合
                  if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
                  {
                    //何もしない
                  }
          //それ以外
                  else
                  {
            //bcst（rh+1）
                    windingHeader.SetSenderNumber (i);
                    rs = i;
                    sendn_rist[id] = id;
                    rh=rh+1;
                    hop = hop+1;
                    windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                    hoptemp[pnm][i] = windingHeader.GetHopCount ();
                    packettemp[pnm][i] = 1;
                  }
                }
        //（else）送信元が左半分の場合
                else if(sendn != RSU_center_number && RSU_center_number > sendn)
                {
                  it = m1.find(sendn); //sender x,y
                  sendposx = it->second;
                  it = m2.find(sendn);
                  sendposy = it->second;
                  it = m1.find(id+1); //next A RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id+1);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          //1つ右側先のRSUが66m以内で受信していたとみなした場合
                  if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                  {
                    //何もしない
                  }
          //それ以外
                  else
                  {
            //bcst（rh+1）
                    windingHeader.SetSenderNumber (i);
                    rs = i;
                    sendn_rist[id] = id;
                    rh=rh+1;
                    hop = hop+1;
                    windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                    hoptemp[pnm][i] = windingHeader.GetHopCount ();
                    packettemp[pnm][i] = 1;
                  }                  
                }
        //（else）送信元が右半分の場合
                else if(sendn != RSU_center_number && sendn > RSU_center_number)
                {
                  it = m1.find(sendn); //sender x,y
                  sendposx = it->second;
                  it = m2.find(sendn);
                  sendposy = it->second;
                  it = m1.find(id+1); //next A RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id+1);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          //1つ左側先のRSUが100m以内で受信していたとみなした場合
                  if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                  {
                    //何もしない
                  }
          //それ以外
                  else
                  {
            //bcst（rh+1）
                    windingHeader.SetSenderNumber (i);
                    rs = i;
                    sendn_rist[id] = id;
                    rh=rh+1;
                    hop = hop+1;
                    windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                    hoptemp[pnm][i] = windingHeader.GetHopCount ();
                    packettemp[pnm][i] = 1;
                  }                     
                }
              }
            }
          }
  //発信元の車両がB方向の時（左側出発の送信車両）
          else
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //送信元が自分より左側の時
            if(sendposx <= nextrsuposx )
            {
      //送信元が山頂RSUの場合で山頂通信範囲内で受信した時
              if(sendn == RSU_center_number && NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                if(HYOUKA_MOUNTAIN_RSU_RANGE == 22500)
                {
                  it = m1.find(id+2); //next B RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id+2);
                  nextrsuposy = it->second;
                } 
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
        //printf(" %d ",id);
        //printf("id+1:%d a:%d ",id+2,a);
        //1つ右側先のRSUが山頂通信距離以内で受信していたとみなした場合
                if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = i;
                    rh = rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //（else）送信元が左半分にいるノードの場合で66m以内で受信した時　かつ　山頂RSUでない場合
              else if(300 > sendposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendn != RSU_center_number)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second; 
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
        //1つ右側先のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = i;
                    rh = rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //（else）送信元が自分より左側にいる　かつ　真ん中より右側にいるの場合で100m以内で受信した時　かつ　山頂RSUでない場合
              else if(sendposx > 300 && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendn != RSU_center_number)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second; 
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
        //1つ右側先のRSUが100m以内で受信していたとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = i;
                    rh = rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
            }
    //自分より右側にいる送信者から66m以内で受信したとみなした場合
            else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx > nextrsuposx)
            {
      //送信元が車両で自分より右側にいる場合
              if((sendn == Nnumber-1 || sendn == Nnumber) && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ右側のRSUが100m以内で受信していたとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop == 1)
                  {
                    rs = i;
                    rh=rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //送信元がRSUで自分より右側にいる場合かつ"rh<=2"の場合
              else if(sendn < Nnumber-9 && rh==2 && hop == 2)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +  (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //1つ左側先のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn_rist[id] = id;
                  rs = i;
                  rh=rh+1;
                  hop = hop+1;
                  //sendn = id;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
            }
          }
        }
      }
    }

//左半分のRSU（山頂,両端ノードを除く）
  for(i = 1; i < RSU_center_number; i++) //のリブロードキャストを防ぐため
  {
    if(id == i) //if Node ID = i
    {
      if(1 != packettemp[pnm][i])
      {
        hoptemp[pnm][i] = windingHeader.GetHopCount (); //格納忘れ防止
        it = vd_rist.find(otemp);
  //発信元の車両がB方向の時(左側出発の送信車両)
        if(1 == it->second)
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //送信元が自分より左側の位置の時で66m以内で受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx )
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
      //右方向側の１つ先のRSUが66m以内で受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                 //何もしない
              }
      //それ以外
              else
              {
        //bcst  if（rh+1）
                windingHeader.SetSenderNumber (i);
                sendn = i;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = i;
                  rh = rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][i] = windingHeader.GetHopCount ();
                packettemp[pnm][i] = 1;
              } 
            }
    //自分より右側にいる送信者から受信したとみなした場合
            else if((NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx > nextrsuposx && sendn != RSU_center_number)
             || (NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE && sendposx > nextrsuposx && sendn == RSU_center_number))
            {
      //送信元が左側出発車両で自分より右側にいる場合
              if(sendn == Nnumber-1 || sendn ==Nnumber)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ右側のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
        //それ以外
                else
                {
          //bcst　if（rh+1）
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop == 1)
                  {
                    rs = i;
                    rh=rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //送信元がRSUで自分より右側にいる場合かつ"rh<=2"の場合
              else if(sendn < Nnumber-9 && rh==2 && hop == 2)
              {
        //送信元が山頂RSUの場合
                if(sendn == RSU_center_number)
                {
                  it = m1.find(sendn); //sender x,y
                  sendposx = it->second;
                  it = m2.find(sendn);
                  sendposy = it->second;
                  it = m1.find(id-1); //next A RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id-1);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          //1つ左側先のRSUが山頂通信距離以内で受信していたとみなした場合
                  if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
                  {
                    //何もしない
                  }
          //それ以外
                  else
                  {
            //bcst（rh+1）
                    windingHeader.SetSenderNumber (i);
                    rs = i;
                    sendn_rist[id] = id;
                    rh=rh+1;
                    hop = hop+1;
                    windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                    hoptemp[pnm][i] = windingHeader.GetHopCount ();
                    packettemp[pnm][i] = 1;
                  }
                }
        //（else）送信元が右半分の場合
                else if(sendn != RSU_center_number && RSU_center_number < sendn)
                {
                  it = m1.find(sendn); //sender x,y
                  sendposx = it->second;
                  it = m2.find(sendn);
                  sendposy = it->second;
                  it = m1.find(id-1); //next A RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id-1);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          //1つ左側先のRSUが66m以内で受信していたとみなした場合
                  if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                  {
                    //何もしない
                  }
          //それ以外
                  else
                  {
            //bcst（rh+1）
                    windingHeader.SetSenderNumber (i);
                    rs = i;
                    sendn_rist[id] = id;
                    rh=rh+1;
                    hop = hop+1;
                    windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                    hoptemp[pnm][i] = windingHeader.GetHopCount ();
                    packettemp[pnm][i] = 1;
                  }                  
                }
        //（else）送信元が左半分の場合
                else if(sendn != RSU_center_number && sendn < RSU_center_number)
                {
                  it = m1.find(sendn); //sender x,y
                  sendposx = it->second;
                  it = m2.find(sendn);
                  sendposy = it->second;
                  it = m1.find(id-1); //next A RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(id-1);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          //1つ左側先のRSUが100m以内で受信していたとみなした場合
                  if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                  {
                    //何もしない
                  }
          //それ以外
                  else
                  {
            //bcst（rh+1）
                    windingHeader.SetSenderNumber (i);
                    rs = i;
                    sendn_rist[id] = id;
                    rh=rh+1;
                    hop = hop+1;
                    windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                    hoptemp[pnm][i] = windingHeader.GetHopCount ();
                    packettemp[pnm][i] = 1;
                  }                     
                }
              }
            }
          }
  //発信元の車両がA方向の時（右側出発の送信車両）
          else
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //送信元が自分より右側の時
            if(sendposx >= nextrsuposx )
            {
      //送信元が山頂RSUの場合で山頂通信範囲内で受信した時
              if(sendn == RSU_center_number && NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                if(HYOUKA_MOUNTAIN_RSU_RANGE == 22500)
                {
                  it = m1.find(id-2); //next B RSU x,y -2
                  nextrsuposx = it->second;
                  it = m2.find(id-2);
                  nextrsuposy = it->second;
                } 
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
        //1つ左側先のRSUが山頂通信距離以内で受信していたとみなした場合
                if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = i;
                    rh = rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //（else）送信元が右半分にいるノードの場合で66m以内で受信した時　かつ　山頂RSUでない場合
              else if(300 < sendposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendn != RSU_center_number)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second; 
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
        //1つ左側先のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = i;
                    rh = rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //（else）送信元が自分より右側にいる　かつ　真ん中より左側にいるの場合で100m以内で受信した時　かつ　山頂RSUでない場合
              else if(sendposx < 300 && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendn != RSU_center_number)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second; 
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
        //1つ左側先のRSUが100m以内で受信していたとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = i;
                    rh = rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
            }
    //自分より左側にいる送信者から66m以内で受信したとみなした場合
            else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx < nextrsuposx)
            {
      //送信元が車両で自分より左側にいる場合
              if((sendn == Nnumber-1 || sendn == Nnumber) && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ左側のRSUが100m以内で受信していたとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn = i;
                  sendn_rist[id] = id;
                  if(hop == 1)
                  {
                    rs = i;
                    rh=rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
      //送信元がRSUで自分より左側にいる場合かつ"rh<=2"の場合
              else if(sendn < Nnumber-9 && rh==2 && hop == 2)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +  (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //1つ右側先のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
                else
                {
                  windingHeader.SetSenderNumber (i);
                  sendn_rist[id] = id;
                  rs = i;
                  rh=rh+1;
                  hop = hop+1;
                  //sendn = id;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][i] = windingHeader.GetHopCount ();
                  packettemp[pnm][i] = 1;
                }
              }
            }
          }
        }
      }
    }






//山頂RSU
  if(id == RSU_center_number)
  {
    if(1 != packettemp[pnm][id])
    {
        hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
        it = vd_rist.find(otemp);
  //発信元の車両がB方向の時(左側出発の送信車両)
        if(1 == it->second)
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);           
    //送信元が自分より左側の位置の時で受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx )
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
      //右方向側の１つ先のRSUが66m以内で受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE) 
              {
                 //何もしない
              }
      //それ以外
              else
              {
        //bcst  if（rh+1）
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = id;
                  rh = rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              } 
            }
    //自分より右側にいる送信者から受信したとみなした場合
            else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx > nextrsuposx)
            {
      //送信元が左側出発車両で自分より右側にいる場合
              if(sendn == Nnumber-1 || sendn ==Nnumber)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ右側のRSUが100m以内で受信していたとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  //何もしない
                }
        //それ以外
                else
                {
          //bcst　if（rh+1）
                  windingHeader.SetSenderNumber (id);
                  sendn = id;
                  sendn_rist[id] = id;
                  if(hop == 1)
                  {
                    rs = id;
                    rh=rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }
      //送信元がRSUで自分より右側にいる場合かつ"rh<=2"の場合
              else if(sendn < Nnumber-9 && rh==2 && hop == 2)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //1つ先の左側のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
        //それ以外
                else
                {
          //bcst（rh+1）
                  windingHeader.SetSenderNumber (id);
                  rs = id;
                  sendn_rist[id] = id;
                  rh=rh+1;
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }
            }
          }
  //発信元の車両がA方向の時（右側出発の送信車両）
          else
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //送信元が自分より右側の位置の時で受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx )
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
      //左方向側の１つ先のRSUが66m以内で受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE) //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              {
                //何もしない
              }
      //それ以外
              else
              {
        //bcst　if（rh+1）
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = id;
                  rh = rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              } 
            }
    //自分より左側にいる送信者から受信したとみなした場合
            else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx < nextrsuposx)
            {
      //送信元が車両で自分より左側にいる場合
              if((sendn == Nnumber-1 || sendn == Nnumber) && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id-1); //next A RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ左側のRSUが100m以内で受信していたとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  //何もしない
                }
        //それ以外
                else
                {
          //bcst　if（rh+1）
                  windingHeader.SetSenderNumber (id);
                  sendn = id;
                  sendn_rist[id] = id;
                  if(hop == 1)
                  {
                    rs = id;
                    rh=rh+1;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }
      //送信元がRSUで自分より左側にいる場合かつ"rh<=2"の場合
              else if(sendn < Nnumber-9 && rh==2 && hop == 2)
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +  (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //1つ先の右側のRSUが66m以内で受信していたとみなした場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  //何もしない
                }
        //それ以外
                else
                {
          //bcst（rh+1）
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  hop = hop+1;
                  //sendn = id;
                  windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }
            }
          }
        }
      }





//if Node ID = 0 左端のRSU
  if(id == 0)
  {
  //ある車両から初めてパケットを受け取った時
    if(vd_rist.find(otemp) == vd_rist.end() )
    {
    //進行方向決定
      if(0 <= windingHeader.GetOriginPositionVectorY () )
      {
        vd_rist[otemp] = 1; //vd_ristにV1の進行方向を格納
        dire = 1; //B方向に向かう車両(上から下)
      }
      else
      {
        vd_rist[otemp] = 0; //vd_ristにV1の進行方向を格納
        dire = 0; //A方向に向かう車両（下から上）
      }
    }
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
  //車両がB方向の場合(送信車両が左側出発)
      if(1 == it->second) 
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
    //自分より左に送信車両がいるとき
          if(sendposx <= nextrsuposx && (sendn == Nnumber-1 && sendn == Nnumber))
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //RSU0が車両から66m以内で受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx )
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分のより1つ右のRSUが66m以内で受信してるとみなした場合
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
          //何もしない
              }
        //自分のより1つ右のRSUが66m以外で受信していないとみなした場合
              else  
              {
          //bcst
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = i;
                  rh = rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop); 
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;                                       
                }      
              }
            }
    //自分より右に送信車両、RSUがいるとき
            else
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //自分より右側の車両から100m以内で受信したとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx >= nextrsuposx && (sendn == Nnumber-1 || sendn == Nnumber) )
              {
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(id+1); //next RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);                            
        //送信車両から自分のより右のRSUが100m以内で受信してるとみなした場合
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
          //何もしない
                }
        //送信車両から自分のより右のRSUが100m以外で受信していないとみなした場合
                else
                {
          //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn = id;
                  sendn_rist[id] = id;
                  if(hop==1)
                  {
                    rs = id;
                    rh=1+rh;
                  }
                  hop = hop+1;
                  windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }              
              }
      //自分より右側のRSUから100m以内で受信した場合 かつ rh=2以下の時
              else if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && Nnumber-1 > sendn && sendn > id && rh == 2 && hop == 2)
              {
        //bcst
                rh = rh+1; //車両の進行方向と反対方向へのホップ数カウント
                windingHeader.SetSenderNumber (id);
                rs = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;                       
              }
            }
          }
  //車両がA方向の時(送信車両が右側出発)          
           else
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU(0) x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +  (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //送信者が自分より右側で100m以内で受信したとみなした時
            if(sendposx >= nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
            {
      //bcst
              windingHeader.SetSenderNumber (id);
              sendn = id;
              sendn_rist[id] = id;
              if(hop == 1)
              {
                rs = i;
                rh = rh+1;
              }
              hop = hop+1;
              windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
              hoptemp[pnm][id] = windingHeader.GetHopCount ();  
              packettemp[pnm][id] = 1;
            }
    //送信者が自分より左側で66以内受信したとみなした時
            else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
      //bcst
              windingHeader.SetSenderNumber (id);
              sendn = id;
              if(hop == 1)
              {
                rs = i;
                rh = rh+1;
              }
              hop = hop+1;
              windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
              hoptemp[pnm][id] = windingHeader.GetHopCount ();  
              packettemp[pnm][id] = 1;
            }
          }
        }
      }


//if Node ID = 右端のRSU
   if(id == Nnumber-10)
   {
  //ある車両から初めてパケットを受け取った時
      if(vd_rist.find(otemp) == vd_rist.end() )
      {
    //進行方向決定 
        if(0 >= windingHeader.GetOriginPositionVectorY () )
        {                           
          vd_rist[otemp] = 0; //vd_ristにV1の進行方向を格納
          dire = 0; //A方向に向かう車両（下から上）
        }
        else
        {
          vd_rist[otemp] = 1; //vd_ristにV1の進行方向を格納
          dire = 1; //B方向に向かう車両(上から下)
        }
      }
      if(1 != packettemp[pnm][Nnumber-10])
      {
        hoptemp[pnm][Nnumber-10] = windingHeader.GetHopCount (); //格納忘れ防止
        //printf("hoptemp[%d][%d] = %d\n",pnm,i,hop);
        it = vd_rist.find(otemp);
  //車両がB方向のとき（送信車両が左側出発）
        if(1 == it->second) //車両がB方向のとき
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU(0) x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //自分より右側にいる送信者から66m以内で受信したとみなした時
          if(sendposx >= nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
          {
      //bcst
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            if(hop == 1)
            {
              rs = id;
              rh = rh+1;
            }
            hop = hop+1;
            windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            hoptemp[pnm][id] = windingHeader.GetHopCount ();  
            packettemp[pnm][id] = 1;
          }
    //自分より左側にいる送信者から100m以内で受信したとみなした時
          else if(sendposx < nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
          {
      //bcst
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            if(hop == 1)
            {
              rs = id;
              rh = rh+1;
            }
            hop = hop+1;
            windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
            hoptemp[pnm][id] = windingHeader.GetHopCount ();  
            packettemp[pnm][id] = 1;
          }
        }
  //車両がA方向のとき（送信車両が右側出発）
        else
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
    //自分より左側に送信車両がいるとき
          if(sendposx <= nextrsuposx && (sendn == Nnumber-1 || sendn == Nnumber))
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //右端RSUが車両から100以内で受信したとみなした時
            if(NEGATIVE_NOMAL_NODE_RANGE < a && a < NOMAL_NODE_RANGE && sendposx <= nextrsuposx )
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より1つ左のRSUが100m以内で受信してるとみなした場合
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
          //何もしない
              }
        //自分のより1つ左のRSUが100m以外で受信していないとみなした場合
              else
              {
          //bcst
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = id;
                  rh = rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;                                       
              }
            }
          }
    //自分より右側に送信車両、RSUがいるとき
          else
          {
            it = m1.find(sendn); //sender x,y
            sendposx = it->second;
            it = m2.find(sendn);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //自分より右側の車両から100m以内で受信したとみなした時
            if(-NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx >= nextrsuposx && (sendn == Nnumber-1 || sendn == Nnumber) ) 
            {
              //printf("send number %d\n",sendn);
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next rsu x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //自分より一つ左のRSUが100m以内で受信したとみなした時
              if(-NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
          //何もしない
              }
        //自分より一つ左のRSUが100m以外で受信したとみなした時
              else
              {
          //bcst
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                if(hop==1)
                {
                  rs = id;
                  rh=rh+1;
                }
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
                //printf("node0 sendWbroadcast seted senderNumber :%d sendn:%d\n",windingHeader.GetSenderNumber (),sendn);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }           
            }
            it = m1.find(rs); //sender x,y
            sendposx = it->second;
            it = m2.find(rs);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //自分より左側のRSUから66m以内で受信した場合 かつ rh=2以下の時
            if(-NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendn < id && rh == 2 ) //自分よりA側のRSUから受信した場合 かつ rh=2以下の時
            {
        //bcst
              rh = rh+1; //車両の進行方向と反対方向へのホップ数カウント
              windingHeader.SetSenderNumber (id);
              rs = id;
              sendn_rist[id] = id;
              //sendn = id;
              hop = hop+1;
              windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this); //現在の時刻からwait_time分だけ待ってSendRBroadcastへ
              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;                       
            }
          }
        }
      }
    }


/*********************************** 送信者下り 対向車の評価 *******************************************/
if(HYOUKA==1)
{
  for(i=0;i<=send_end_count;i++)
  {
    if(send_zyun_node[i] != 100)
    {
      if(id == Nnumber) //destination node
      {
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber-1); //source車両 x,y
                        int sendposx = it->second;
                        it = m2.find(Nnumber-1);
                        int sendposy = it->second;
                        it = m1.find(id); //destination x,y
                        int nextrsuposx = it->second;
                        it = m2.find(id);
                        int nextrsuposy = it->second;
                        int b = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx); //source car no tuusinhanni ni hairumade
                        if(nextrsuposx > sendposx && 500 > sendposx && sendposx >= HYOKA_KUKAN && b >=HYOUKA_END_DISTANCE && otemp == Nnumber-1 ) //source車両とdestinationがすれ違うまで
                        //if(100 < nextrsuposx && nextrsuposx < sendposx && otemp == Nnumber-1 && b >=10000) //source車両とdestinationがすれ違うまで
                        {

                                it = m1.find(send_zyun_node[i]); //sender x,y
                                sendposx = it->second;
                                it = m2.find(send_zyun_node[i]);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                    if(send_zyun_node[i]==RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
                                    {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id] << " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                    }
                                   if(send_zyun_node[i] != RSU_center_number && nextrsuposx <= sendposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                   }
                                   if(send_zyun_node[i] != RSU_center_number &&  nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id];
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
//printf("%d",sendn_rist[j]);
                                   }

                        }
                }  
      }
    }
  }
}
/*********************************** 送信者上り 対向車の評価 *******************************************/
/*
        if(id == Nnumber-1) //destination
        {
//printf("packettemp[%d][%d] = %d ",pnm,id,packettemp[pnm][id]);
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber); //source車両 x,y
                        sendposx = it->second;
                        it = m2.find(Nnumber);
                        sendposy = it->second;
                        it = m1.find(id); //my x,y
                        nextrsuposx = it->second;
                        it = m2.find(id);
                        nextrsuposy = it->second;
                        //printf("sendx:%d sendy:%d my x:%d my y%d   ",sendposx,sendposy,nextrsuposx,nextrsuposy);
                        if(nextrsuposx < sendposx && 500 > sendposx && sendposx >= 100) //source車両とdestinationがすれ違うまで
                        {
                                it = m1.find(sendn); //sender x,y
                                sendposx = it->second;
                                it = m2.find(sendn);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                //printf("sendx:%d sendy:%d my x:%d my y%d a:%d\n\n",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
                                //printf("sendx:%d sendy:%d my x:%d my y%d a:%d   ",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
                                if(nextrsuposx >= sendposx && -10000 <= a && a <= 10000 && otemp == Nnumber)
                                {
                                        std::cout<<"　"<< Simulator::Now ();
                                        //printf("recv packet!!\n");
                                        packettemp[pnm][id] = 1;
                                        
                                }
                                if(nextrsuposx < sendposx && -4444 <= a && a <= 4444 && otemp==Nnumber)
                                {
                                        std::cout<<"　"<< Simulator::Now ();
                                        //printf("recv packet!!\n");
                                        packettemp[pnm][id] = 1;
                                }
                        }
                }   
        }
*/


/************************************ パケット受信数のカウント **************************************/


  for(i = Nnumber-9; i <= Nnumber; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
        for(n=0; n<=50; n++)
        {
          if(sendn_rist[n] != 100)
          {
  //受け取ったパケットの送信元リストにのっていない場合（車両のパケット送信毎にリストを初期化している）
            if(recv_rist[id][sendn_rist[n]] != 1)
            {
              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元ノードが山頂RSUの場合はそのままの通信範囲で受信成功とみなす
    	        if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //受信車両の位置が真ん中より右側の場合
              else if(300 < nextrsuposx)
              {
    //送信元が自分より左側で真ん中より右側の場合は距離が100m以内だと通信成功とみなす
                if(300 < sendposx && sendposx < nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
  //受信車両の位置が真ん中より左側の場合
              else if(300 > nextrsuposx)
              {
    //送信元が自分より右側で真ん中より左側の場合は距離が100m以内だと通信成功とみなす
                if(300 > sendposx && sendposx > nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
            }
          }
        }
      }
    }
  }



//左側のRSUノードである
  for(i = 0; i <= RSU_center_number-1; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より左側の場合はお互いの距離が６６m以内だと受信成功とみなす
              else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードかつ、自分より右側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx < 300 && nextrsuposx < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
      }
    }
  }

//真ん中のRSUノードである
  if(id == RSU_center_number)
  {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
    for(n=0; n<=50; n++)
    {
		  if(sendn_rist[n] != 100)
		  {
      if(recv_rist[id][sendn_rist[n]] != 1)
{        
  //送信元の全ノードが66m以内だと受信成功とみなす→bcst
        it = m1.find(sendn_rist[n]); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn_rist[n]);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
        {
          packetcnt++;
          recv_rist[id][sendn_rist[n]] = 1;
        }
      }
    }
    }
  }
  }

//右側のRSUノードである
  for(i = RSU_center_number+1; i <= Nnumber-10; i++)
  {
    if(id == i )
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より右側の場合はお互いの距離が６６m以内だと受信成功とみなす→bcst
              else if(sendposx > nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードかつ、自分より左側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx > 300 && nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
    }
    }
  }

/**************************************下り車両から後方車両へ********************************************/
if(HYOUKA == 2)
{
  int j;
  for(j=0;j<=send_end_count;j++)
  {
    if(send_zyun_node[j] != 100)
    {
      if(Nnumber-9 <= id && id <= Nnumber-6)
      {
        if(1 != packettemp_t[pnm][id])
        {
          GetDestPosition();
          it = m1.find(Nnumber-1); //source車両 x,y
          sendposx = it->second;
          it = m2.find(Nnumber-1);
          sendposy = it->second;
          if(500 > sendposx && sendposx >= 100 && otemp == Nnumber-1) //source車両が峠道区間をぬけるまで
          {
            it = m1.find(send_zyun_node[j]); //source車両 x,y
            sendposx = it->second;
            it = m2.find(send_zyun_node[j]);
            sendposy = it->second;
            it = m1.find(id); //my  x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
//送信ノードが真ん中にいる（送信車両が山頂RSUノード）
            if(send_zyun_node[j] == RSU_center_number)
            {
  //山頂RSUの通信距離内なら受信成功
              if(HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
//送信ノードが左側にいる（送信車両も含む）
            else if(sendposx < 300)
            {
  //送信ノードより受信ノードの方が左側の場合
              if(nextrsuposx < sendposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id] )
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                  packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              } 
            }
//送信ノードが右にいる（送信車両も含む）
            else if(300 < sendposx)
            {
  //送信ノードより受信ノードの方が右側の場合
              if(sendposx < nextrsuposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id])
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                 //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                 packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功     
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
          }
        }
      }
    }
  }
}
/*********************************上り車両から後方車両へ***************************************/
/*
  int j;
  for(j=0;j<=Nnumber;j++)
  {
    if(sendn_rist[j] != 100)
    {
      for(i = Nnumber-5; i <= Nnumber-2; i++) //のリブロードキャストを防ぐため
      {
        if(i==id) //destination
        {
//printf("packettemp[%d][%d] = %d ",pnm,id,packettemp[pnm][id]);
                if(1 != packettemp[pnm][id])
                {
                        GetDestPosition();
                        it = m1.find(Nnumber); //source車両 x,y
                        sendposx = it->second;
                        it = m2.find(Nnumber);
                        sendposy = it->second;
                        //printf("sendx:%d sendy:%d my x:%d my y%d   ",sendposx,sendposy,nextrsuposx,nextrsuposy);
                        if(500 > sendposx && sendposx >= 100 && otemp == Nnumber) //source車両が峠道区間をぬけるまで
                        {
                                it = m1.find(sendn_rist[j]); //sender x,y
                                sendposx = it->second;
                                it = m2.find(sendn_rist[j]);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                   if(nextrsuposx >= sendposx && -10000 <= a && a <= 10000 )
                                   {
        
                                           std::cout<<"　"<< Simulator::Now ();
                                           //printf("recv packet!!\n");
                                           packettemp[pnm][id] = 1;
 //printf("%d",sendn_rist[j]);                                          
                                   }
                                   else if(nextrsuposx < sendposx && -4444 <= a && a <= 4444 )
                                   {
        
                                           std::cout<<"　"<< Simulator::Now ();
                                           //printf("recv packet!!\n");
                                           packettemp[pnm][id] = 1;
//printf("%d",sendn_rist[j]);
                                   }
                        }
                }   
        }
      }
    }
  }
*/
} //RecvWindingBroadcast


/***********************************    winding2    *******************************************/
void
RoutingProtocol::RecvWinding2Broadcast()
{

  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
  int i = 0;
  int sendposx,sendposy;
  int nextrsuposx,nextrsuposy;
  //int myposy,myposx;
  int a;
  WindingHeader windingHeader;
  std::map<int,int>::iterator it;
/*
if(id <= Nnumber-10)
{
        it = m1.find(RSU_center_number); //sender x,y
        sendposx = it->second;
        it = m2.find(RSU_center_number);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_MOUNTAIN_RSU_RANGE <=a && a <= MOUNTAIN_RSU_RANGE)
        {
          printf(" %d ",id);
        }
        
          printf(" %d ",id);

}
*/


//if Node ID = 右半分RSU
  if(RSU_center_number < id && id < Nnumber-10)
  {
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
  //発信元の車両がA方向の時
      if(0 == it->second)
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //B側のノードからで受信したとみなした時
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && nextrsuposx <= sendposx)
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
      //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
          {
              //何もしない
          }
          else
          {
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;
          } 
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //A側のノードである
        if(sendposx < nextrsuposx)
        {
      //送信元が車両で自分よりA側にいる場合
          if(otemp == Nnumber-1 || otemp ==Nnumber)
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id+1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id+1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //送信元が真ん中より左側の場合
            if(sendposx < 300)
            {
          //66m以内で１つ右側のRSUが受信したとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber)
              {
                windingHeader.SetSenderNumber (id);
                sendn_rist[id] = id;
                rs = id;
                rh=rh+1;
                windingHeader.SetHopCount (hop);  
                            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
        //送信元が真ん中より右側の場合
            else
            {
          //100m以内で１つ右側のRSUが受信したとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber)
              {
                windingHeader.SetSenderNumber (id);
                sendn_rist[id] = id;
                rs = id;
                rh=rh+1;
                windingHeader.SetHopCount (hop);  
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }              
            }            
          }
        }
      }
      else //発信元の車両がB方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //自分よりA側にいるノードが送信元の場合
        if(sendposx < nextrsuposx)
        {
      //山頂RSUの場合
          if(sendn == RSU_center_number)
          {
        //山頂通信距離以内で受信したとみなした場合
            if(NEGATIVE_MOUNTAIN_RSU_RANGE <=a && a <= MOUNTAIN_RSU_RANGE)
            {
              //printf(" %d:%d ",id,a);
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
                it = m1.find(id+1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id+1);
                nextrsuposy = it->second;
                if(HYOUKA_MOUNTAIN_RSU_RANGE == 22500)
                {
                  it = m1.find(id+2); //next B RSU x,y +2
                  nextrsuposx = it->second;
                  it = m2.find(id+2);
                  nextrsuposy = it->second;
                } 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
              //printf(" %d:%d ",id,a);              
          //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }            
            }
          }
      //山頂RSU以外で左半分にいるノードの場合
          if(sendn != RSU_center_number && sendposx <= 300)
          {
        //66m以内で受信したとみなした場合
            if(NEGATIVE_UPHILL_RANGE <=a && a <= UPHILL_RANGE)
            {
          //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
      //山頂RSU以外で右半分にいるノードの場合
          if(sendn != RSU_center_number && 300 < sendposx)
          {
        //100m以内で受信したとみなした場合
            if(NEGATIVE_NOMAL_NODE_RANGE <=a && a <= NOMAL_NODE_RANGE)
            {
          //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //B側のノードから受信したとみなした場合
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx > nextrsuposx)
        {
    //送信元が車両で自分よりB側にいる場合
          if(otemp == Nnumber-1 || otemp ==Nnumber)
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id-1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id-1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //66m以内で１つ左側のRSUが受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
                //何もしない
            }
            else if(rh<=1 && otemp == Nnumber-1)
            {
              windingHeader.SetSenderNumber (id);
              sendn_rist[id] = id;
              rs = id;
              rh=rh+1;
              windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
          send_zyun_node[send_zyun_node_count] = id;
          Time wait_time = Time(250.0);
          Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
          }
        }
      }
    }
  }



//if Node ID = 左半分RSU
for(i=1; i<RSU_center_number; i++)
{
  if(id == i)
  {
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
      if(1 == it->second) //発信元の車両がB方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx) //A側のノードからで受信したとみなした時
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE) //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
          {
              //何もしない
          }
          else
          {
            //printf("rebroadcast a:%d\n",a);
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;
          } 
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //B側のノードの時
        if(sendposx > nextrsuposx)
        {
      //送信元が車両で自分よりB側にいる場合
          if(otemp == Nnumber-1 || otemp ==Nnumber)
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id-1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id-1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //送信元が真ん中より右側の場合        
            if(sendposx > 300)
            {
          //66m以内で１つ左側のRSUが受信したとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber-1)
              {
                windingHeader.SetSenderNumber (id);
                sendn_rist[id] = id;
                rs = id;
                rh=rh+1;
                windingHeader.SetHopCount (hop);  
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
            else
            {
          //100m以内で１つ左側のRSUが受信したとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber-1)
              {
                windingHeader.SetSenderNumber (id);
                sendn_rist[id] = id;
                rs = id;
                rh=rh+1;
                windingHeader.SetHopCount (hop);  
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }              
            }            
          }
        }
      }
      else //発信元の車両がA方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //自分よりB側にいるノードが送信元の場合
        if(sendposx > nextrsuposx)
        {
      //山頂RSUの場合
          if(sendn == RSU_center_number)
          {
        //山頂通信距離以内で受信したとみなした場合
            if(NEGATIVE_MOUNTAIN_RSU_RANGE <=a && a <= MOUNTAIN_RSU_RANGE)
            {
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
                it = m1.find(id-1); //next B RSU x,y
                nextrsuposx = it->second;
                it = m2.find(id-1);
                nextrsuposy = it->second;
                if(HYOUKA_MOUNTAIN_RSU_RANGE == 22500)
                {
                  it = m1.find(id-2); //next B RSU x,y -2
                  nextrsuposx = it->second;
                  it = m2.find(id-2);
                  nextrsuposy = it->second;
                } 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }            
            }
          }
      //山頂RSU以外で右半分にいるノードの場合
          else if(sendn != RSU_center_number && 300 <= sendposx)
          {
        //66m以内で受信したとみなした場合
            if(NEGATIVE_UPHILL_RANGE <=a && a <= UPHILL_RANGE)
            {
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
      //山頂RSU以外で左半分にいるノードの場合
          else if(sendn != RSU_center_number && sendposx < 300)
          {
        //100m以内で受信したとみなした場合
            if(NEGATIVE_NOMAL_NODE_RANGE <=a && a <= NOMAL_NODE_RANGE)
            {
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                 send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx < nextrsuposx) //A側のノードから受信したとみなした場合
        {
          if(otemp == Nnumber-1 || otemp ==Nnumber) //送信元が車両で自分よりA側にいる場合
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id+1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id+1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //66m以内で１つ右側のRSUが受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
                //何もしない
            }
            else if(rh<=1 && otemp == Nnumber)
            {
              windingHeader.SetSenderNumber (id);
              sendn_rist[id] = id;
              rs = id;
              rh=rh+1;
              windingHeader.SetHopCount (hop);  
                send_zyun_node_count = send_zyun_node_count+1;
          send_zyun_node[send_zyun_node_count] = id;
          Time wait_time = Time(250.0);
          Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
          }
        }
      }
    }
  }
}



  if(id == 0) /// if Node ID = 0 端のRSU
  {
    if(vd_rist.find(otemp) == vd_rist.end() ) //ある車両から初めてパケットを受け取った時
    {
      if(0 <= windingHeader.GetOriginPositionVectorY () ) //進行方向決定
      {
        vd_rist[otemp] = 1; //vd_ristにV1の進行方向を格納
        dire = 1; //B方向に向かう車両(上から下)
        //printf("dddddddddddddddddddddddddddddddddd1 %d\n",dire);
      }
      else
      {
        vd_rist[otemp] = 0; //vd_ristにV1の進行方向を格納
        dire = 0; //A方向に向かう車両（下から上）
        //printf("dddddddddddddddddddddddddddddddddd1 %d\n",dire);
      }
    }
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
  //車両がB方向の場合
      if(1 == it->second)  
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
    // A側のノード(車両)から受信したとみなした時
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx )
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //next RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //送信車両から自分のより1つ先のRSUが受信してるとみなした場合
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && rh<=1)
          {
            //逆方向へのホップ
            windingHeader.SetSenderNumber (id);
            sendn_rist[id] = id;
            rs = id;
            rh=1+rh;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                         
          }
      //送信車両から自分のより1つ先のRSUが受信していないとみなした場合
          else
          {
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            rh=1+rh; //不要なリブロードキャストをなくすため
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                                       
          }
        }
        it = m1.find(Nnumber-1); //sender x,y
        sendposx = it->second;
        it = m2.find(Nnumber-1);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
    //送信車両がB側にいて受信している場合
        if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx > nextrsuposx && rh<=1) //&& sendn==Nnumber-1)
        {
          //逆方向へのホップ
          windingHeader.SetSenderNumber (id);
          sendn_rist[id] = id;
          rs = id;
          rh=1+rh;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();
          packettemp[pnm][id] = 1;  
        }
      }
      else //車両の方向がA方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU(0) x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
        (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(sendposx >= nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE) //自分よりB側にいる送信者から受信したとみなした時
        {
          windingHeader.SetSenderNumber (id);
          sendn = id;
          sendn_rist[id] = id;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();  
          packettemp[pnm][id] = 1;
        }
        else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE ) //自分よりA側にいる送信者から受信したとみなした時
        { 
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //my RSU(0) x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          if(NEGATIVE_UPHILL_RANGE <= a && a<= UPHILL_RANGE)
          {
          }
          else if(rh <= 1) //逆方向へのホップ
          {
            windingHeader.SetSenderNumber (id); 
            sendn_rist[id] = id;
            rs = id;
            rh = rh+1;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();  
            packettemp[pnm][id] = 1;
          }
        }
      }
    }
  }



//山頂RSU
  if(id == RSU_center_number) //if Node ID = i
  {
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
      if(1 == it->second) //発信元の車両がB方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx) //A側のノードからで受信したとみなした時
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE) //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
          {
              //何もしない
          }
          else
          {
            //printf("rebroadcast a:%d\n",a);
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;
          } 
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx > nextrsuposx) //B側のノードから受信したとみなした場合
        {
          if(otemp == Nnumber-1 || otemp ==Nnumber) //送信元が車両で自分よりB側にいる場合
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id-1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id-1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
            //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
              //何もしない
            }
            else if(rh<=1 && otemp == Nnumber-1)
            {
              windingHeader.SetSenderNumber (id);
              sendn_rist[id] = id;
              rs = id;
              rh=rh+1;
              windingHeader.SetHopCount (hop);  
               send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
          }
        }
      }
      else //発信元の車両がA方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx) //B側のノードからで受信したとみなした時
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE) //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
          {
              //何もしない
          }
          else
          {
            //printf("rebroadcast a:%d\n",a);
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;
          } 
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx < nextrsuposx) //A側のノードから受信したとみなした場合
        {
          if(otemp == Nnumber-1 || otemp ==Nnumber) //送信元が車両で自分よりA側にいる場合
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id+1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id+1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
            //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
                //何もしない
            }
            else if(rh<=1 && otemp == Nnumber)
            {
              windingHeader.SetSenderNumber (id);
              sendn_rist[id] = id;
              rs = id;
              rh=rh+1;
              windingHeader.SetHopCount (hop);  
               send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
          }
        }
      }
    }
  }
    
  if(id == Nnumber-10) ///if Node ID = Nnumber-10 端のRSU
  {
    if(vd_rist.find(otemp) == vd_rist.end() ) //ある車両から初めてパケットを受け取った時
    {
      if(0 >= windingHeader.GetOriginPositionVectorY () ) //進行方向決定 
      {                           
        vd_rist[otemp] = 0; //vd_ristにV1の進行方向を格納
        dire = 0; //A方向に向かう車両（下から上）
        //printf("dddddddddddddddddddddddddddddddddd2 %d\n",dire);
      }
      else
      {
        vd_rist[otemp] = 1; //vd_ristにV1の進行方向を格納
        dire = 1; //B方向に向かう車両(上から下)
        //printf("dddddddddddddddddddddddddddddddddd2 %d\n",dire);
      }
    }
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      //printf("hoptemp[%d][%d] = %d\n",pnm,i,hop);
      it = vd_rist.find(otemp);
      if(1 == it->second) //車両がB方向のとき
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU(0) x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(sendposx <= nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE) //自分よりA側にいる送信者から受信したとみなした時
        {
          windingHeader.SetSenderNumber (id);
          sendn = id;
          sendn_rist[id] = id;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();  
          packettemp[pnm][id] = 1;
        }
        if(sendposx > nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE ) //自分よりB側にいる送信者から受信したとみなした時
        { 
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //my RSU(0) x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          if(NEGATIVE_UPHILL_RANGE <= a && a<= UPHILL_RANGE)
          {
          }
          else if(rh <= 1) //逆方向へのホップ
          {
            windingHeader.SetSenderNumber (id); 
            sendn_rist[id] = id;
            rs = id;
            rh = rh+1;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();  
            packettemp[pnm][id] = 1;
          }
        }
      }
      else //車両がA方向のとき
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx ) // B側のノード(車両)から受信したとみなした時
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //next RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && rh<=1) //送信車両から自分のより1つ先のRSUが受信してるとみなした場合
          {
            //逆方向へのホップ
            windingHeader.SetSenderNumber (id);
            sendn_rist[id] = id;
            rs = id;
            rh=1+rh;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                                      
          }
          else  ////送信車両から自分のより1つ先のRSUが受信していないとみなした場合
          {
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            rh=1+rh; //不要なリブロードキャストをなくすため
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                                       
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx < nextrsuposx && rh<=1 && otemp==Nnumber)
        {
          windingHeader.SetSenderNumber (id);
          sendn_rist[id] = id;
          rs = id;
          rh=1+rh;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();
          packettemp[pnm][id] = 1;
        }                        
      }
    }
  }

/*********************************** 送信者下り 対向車の評価 *******************************************/
if(HYOUKA == 1)
{
  for(i=0;i<=send_end_count;i++)
  {
    if(send_zyun_node[i] != 100)
    {
      if(id == Nnumber) //destination node
      {
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber-1); //source車両 x,y
                        int sendposx = it->second;
                        it = m2.find(Nnumber-1);
                        int sendposy = it->second;
                        it = m1.find(id); //destination x,y
                        int nextrsuposx = it->second;
                        it = m2.find(id);
                        int nextrsuposy = it->second;
                        int b = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx); //source car no tuusinhanni ni hairumade
                        if(nextrsuposx > sendposx && 500 > sendposx && sendposx >= HYOKA_KUKAN && b >=HYOUKA_END_DISTANCE && otemp == Nnumber-1 ) //source車両とdestinationがすれ違うまで
                        //if(100 < nextrsuposx && nextrsuposx < sendposx && otemp == Nnumber-1 && b >=10000) //source車両とdestinationがすれ違うまで
                        {

                                it = m1.find(send_zyun_node[i]); //sender x,y
                                sendposx = it->second;
                                it = m2.find(send_zyun_node[i]);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                    if(send_zyun_node[i]==RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
                                    {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id] << " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                    }
                                   if(send_zyun_node[i] != RSU_center_number && nextrsuposx <= sendposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                   }
                                   if(send_zyun_node[i] != RSU_center_number &&  nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id];
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
//printf("%d",sendn_rist[j]);
                                   }

                        }
                }  
      }
    }
  }
}
/*********************************** 送信者上り 対向車の評価 *******************************************/
/*
        if(id == Nnumber-1) //destination
        {
//printf("packettemp[%d][%d] = %d ",pnm,id,packettemp[pnm][id]);
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber); //source車両 x,y
                        sendposx = it->second;
                        it = m2.find(Nnumber);
                        sendposy = it->second;
                        it = m1.find(id); //my x,y
                        nextrsuposx = it->second;
                        it = m2.find(id);
                        nextrsuposy = it->second;
                        //printf("sendx:%d sendy:%d my x:%d my y%d   ",sendposx,sendposy,nextrsuposx,nextrsuposy);
                        if(nextrsuposx < sendposx && 500 > sendposx && sendposx >= 100) //source車両とdestinationがすれ違うまで
                        {
                                it = m1.find(sendn); //sender x,y
                                sendposx = it->second;
                                it = m2.find(sendn);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                //printf("sendx:%d sendy:%d my x:%d my y%d a:%d\n\n",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
                                //printf("sendx:%d sendy:%d my x:%d my y%d a:%d   ",sendposx,sendposy,nextrsuposx,nextrsuposy,a);
                                if(nextrsuposx >= sendposx && -10000 <= a && a <= 10000 && otemp == Nnumber)
                                {
                                        std::cout<<"　"<< Simulator::Now ();
                                        //printf("recv packet!!\n");
                                        packettemp[pnm][id] = 1;
                                        
                                }
                                if(nextrsuposx < sendposx && -4444 <= a && a <= 4444 && otemp==Nnumber)
                                {
                                        std::cout<<"　"<< Simulator::Now ();
                                        //printf("recv packet!!\n");
                                        packettemp[pnm][id] = 1;
                                }
                        }
                }   
        }

*/
/*************************************** パケット受信数のカウント ************************************/

int n=0;
  for(i = Nnumber-9; i <= Nnumber; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
        for(n=0; n<=50; n++)
        {
          if(sendn_rist[n] != 100)
          {
  //受け取ったパケットの送信元リストにのっていない場合（車両のパケット送信毎にリストを初期化している）
            if(recv_rist[id][sendn_rist[n]] != 1)
            {
              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元ノードが山頂RSUの場合はそのままの通信範囲で受信成功とみなす
    	        if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //受信車両の位置が真ん中より右側の場合
              else if(300 < nextrsuposx)
              {
    //送信元が自分より左側で真ん中より右側の場合は距離が100m以内だと通信成功とみなす
                if(300 < sendposx && sendposx < nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
  //受信車両の位置が真ん中より左側の場合
              else if(300 > nextrsuposx)
              {
    //送信元が自分より右側で真ん中より左側の場合は距離が100m以内だと通信成功とみなす
                if(300 > sendposx && sendposx > nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
            }
          }
        }
      }
    }
  }



//左側のRSUノードである
  for(i = 0; i <= RSU_center_number-1; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より左側の場合はお互いの距離が６６m以内だと受信成功とみなす
              else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードかつ、自分より右側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx < 300 && nextrsuposx < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
      }
    }
  }

//真ん中のRSUノードである
  if(id == RSU_center_number)
  {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
    for(n=0; n<=50; n++)
    {
		  if(sendn_rist[n] != 100)
		  {
      if(recv_rist[id][sendn_rist[n]] != 1)
{        
  //送信元の全ノードが66m以内だと受信成功とみなす→bcst
        it = m1.find(sendn_rist[n]); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn_rist[n]);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
        {
          packetcnt++;
          recv_rist[id][sendn_rist[n]] = 1;
        }
      }
    }
    }
  }
  }

//右側のRSUノードである
  for(i = RSU_center_number+1; i <= Nnumber-10; i++)
  {
    if(id == i )
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より右側の場合はお互いの距離が６６m以内だと受信成功とみなす→bcst
              else if(sendposx > nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードかつ、自分より左側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx > 300 && nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
    }
    }
  }

/**************************************下り車両から後方車両へ********************************************/
if(HYOUKA == 2)
{
  int j;
  for(j=0;j<=send_end_count;j++)
  {
    if(send_zyun_node[j] != 100)
    {
      if(Nnumber-9 <= id && id <= Nnumber-6)
      {
        if(1 != packettemp_t[pnm][id])
        {
          GetDestPosition();
          it = m1.find(Nnumber-1); //source車両 x,y
          sendposx = it->second;
          it = m2.find(Nnumber-1);
          sendposy = it->second;
          if(500 > sendposx && sendposx >= 100 && otemp == Nnumber-1) //source車両が峠道区間をぬけるまで
          {
            it = m1.find(send_zyun_node[j]); //source車両 x,y
            sendposx = it->second;
            it = m2.find(send_zyun_node[j]);
            sendposy = it->second;
            it = m1.find(id); //my  x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
//送信ノードが真ん中にいる（送信車両が山頂RSUノード）
            if(send_zyun_node[j] == RSU_center_number)
            {
  //山頂RSUの通信距離内なら受信成功
              if(HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
//送信ノードが左側にいる（送信車両も含む）
            else if(sendposx < 300)
            {
  //送信ノードより受信ノードの方が左側の場合
              if(nextrsuposx < sendposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id] )
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                  packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              } 
            }
//送信ノードが右にいる（送信車両も含む）
            else if(300 < sendposx)
            {
  //送信ノードより受信ノードの方が右側の場合
              if(sendposx < nextrsuposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id])
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                 //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                 packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功     
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
          }
        }
      }
    }
  }
}


/*********************************上り車両から後方車両へ***************************************/
/*
  int j;
  for(j=0;j<=Nnumber;j++)
  {
    if(sendn_rist[j] != 100)
    {
      for(i = Nnumber-5; i <= Nnumber-2; i++) //のリブロードキャストを防ぐため
      {
        if(i==id) //destination
        {
//printf("packettemp[%d][%d] = %d ",pnm,id,packettemp[pnm][id]);
                if(1 != packettemp[pnm][id])
                {
                        GetDestPosition();
                        it = m1.find(Nnumber); //source車両 x,y
                        sendposx = it->second;
                        it = m2.find(Nnumber);
                        sendposy = it->second;
                        //printf("sendx:%d sendy:%d my x:%d my y%d   ",sendposx,sendposy,nextrsuposx,nextrsuposy);
                        if(500 > sendposx && sendposx >= 100 && otemp == Nnumber) //source車両が峠道区間をぬけるまで
                        {
                                it = m1.find(sendn_rist[j]); //sender x,y
                                sendposx = it->second;
                                it = m2.find(sendn_rist[j]);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                   if(nextrsuposx >= sendposx && -10000 <= a && a <= 10000 )
                                   {
        
                                           std::cout<<"　"<< Simulator::Now ();
                                           //printf("recv packet!!\n");
                                           packettemp[pnm][id] = 1;
 //printf("%d",sendn_rist[j]);                                          
                                   }
                                   else if(nextrsuposx < sendposx && -4444 <= a && a <= 4444 )
                                   {
        
                                           std::cout<<"　"<< Simulator::Now ();
                                           //printf("recv packet!!\n");
                                           packettemp[pnm][id] = 1;
//printf("%d",sendn_rist[j]);
                                   }
                        }
                }   
        }
      }
    }
  }
*/


} //winding2

/***********************************    winding3    *******************************************/
void
RoutingProtocol::RecvWinding3Broadcast()
{

  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
  int i = 0;
  int sendposx,sendposy; //送信元の座標
  int nextrsuposx,nextrsuposy; //先のRSU座標

  //int myposy,myposx;
  int a;
  int b; //通信範囲比較用
  WindingHeader windingHeader;
  std::map<int,int>::iterator it;



  int next_rsu_count = 0; //受信した最も遠いRSUを判別するためのRSUカウンタ
  int farthest_rsu = RSU_center_number; //受信した最も遠いRSU番号



int hikaku_range;
int n_hikaku_range;
if(MOUNTAIN_RSU_RANGE == 10000)
{
  hikaku_range = 10000;
  n_hikaku_range = -10000;
}
if(MOUNTAIN_RSU_RANGE == 15625)
{
  hikaku_range = 15625;
  n_hikaku_range = -15625;
}
if(MOUNTAIN_RSU_RANGE == 22500)
{
  hikaku_range = 19000;
  n_hikaku_range = -19000;
}






//if Node ID = 右半分のRSU
for(i=RSU_center_number+1; i<Nnumber-10; i++)
{
  if(id == i)
  {
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
  //発信元の車両がA方向の時
      if(0 == it->second)
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //B側のノードからで受信したとみなした時
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx)
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
      //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
          {
        //何もしない
          }
      //それ以外
          else
          {
        //bcst
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //A側のノードの時
        if(sendposx < nextrsuposx)
        {
      //送信元が車両
          if(otemp == Nnumber-1 || otemp ==Nnumber)
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //送信元が真ん中より左側の場合で受信した時
            if(sendposx < 300 && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
            it = m1.find(id+1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id+1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);              
          //66m以内で１つ右側のRSUが受信したとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
            //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber-1)
              {
                it = m1.find(otemp); //sender x,y
                sendposx = it->second;
                it = m2.find(otemp);
                sendposy = it->second;
                it = m1.find(RSU_center_number); //center RSU x,y
                nextrsuposx = it->second;
                it = m2.find(RSU_center_number);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            //送信元から山頂RSUに送信できている場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
              //山頂RSUと自分(山頂RSUと通常RSUの距離　＋　通常RSUの通信距離100m)の通信範囲を比較
                  it = m1.find(id); //id x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(RSU_center_number); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(RSU_center_number);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
              //自分 < 山頂RSUの場合
                  if(n_hikaku_range < b && b < hikaku_range)
                  {
                //何もしない（山頂RSUがBCSTしたと予測して）
                  }
              //自分 > 山頂RSUの場合
                  else
                  {
                //BCST
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }
            //山頂RSUが受信していない場合
                }
                else
                {
              //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;                
                }
              }
            }
        //送信元が真ん中より右側の場合で受信した時
            else if(sendposx >= 300 && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE )
            {
          //100m以内で１つ左側のRSUが受信したとみなした時
              it = m1.find(otemp); //sender x,y
              sendposx = it->second;
              it = m2.find(otemp);
              sendposy = it->second;
              it = m1.find(id-1); //center RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //100m以内で１つ右側のRSUが受信したとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
            //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber-1)
              {
                it = m1.find(otemp); //sender x,y
                sendposx = it->second;
                it = m2.find(otemp);
                sendposy = it->second;
                it = m1.find(RSU_center_number); //center RSU x,y
                nextrsuposx = it->second;
                it = m2.find(RSU_center_number);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            //送信元から山頂RSUに送信できている場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
              //山頂RSUと自分(山頂RSUと通常RSUの距離　＋　通常RSUの通信距離100m)の通信範囲を比較
                  it = m1.find(id); //id x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(RSU_center_number); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(RSU_center_number);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
              //自分 < 山頂RSUの場合
                  if(n_hikaku_range < b && b < hikaku_range)
                  {
                //何もしない（山頂RSUがBCSTしたと予測して）
                  }
              //自分 > 山頂RSUの場合
                  else
                  {
                //BCST
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }
            //山頂RSUが受信していない場合
                }
                else
                {
              //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }         
            }          
          }
        }
      }
  //発信元の車両がB方向の時
      else
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //自分よりA側にいるノードが送信元の場合
        if(sendposx <= nextrsuposx)
        {
      //山頂RSUの場合
          if(sendn == RSU_center_number)
          {
        //山頂通信距離以内で受信したとみなした場合
            if(NEGATIVE_MOUNTAIN_RSU_RANGE <=a && a <= MOUNTAIN_RSU_RANGE)
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second;
              if(HYOUKA_MOUNTAIN_RSU_RANGE == 22500)
              {
                it = m1.find(id+2); //next B RSU x,y -2
                nextrsuposx = it->second;
                it = m2.find(id+2);
                nextrsuposy = it->second;
              }
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(n_hikaku_range <= a && a <= hikaku_range)
              {
                  //何もしない
              }
          //それ以外
              else
              {
            //bcst
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
                Time wait_time = Time(250.0);
                Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }            
            }
          }
      //山頂RSU以外で左半分にいるノードの場合
          else if(sendn != RSU_center_number && 300 >= sendposx)
          {
        //66m以内で受信したとみなした場合
        //printf(" a ");
            if(NEGATIVE_UPHILL_RANGE <=a && a <= UPHILL_RANGE)
            {
              //printf(" id:%d fr:%d a:%d ",id,sendn,a);
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
             //printf(" a+1:%d ",a);
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
            //何もしない
            //printf(" b ");
              }
              else
          //それ以外
              {
                //printf("c");
                it = m1.find(sendn); //sender x,y
                sendposx = it->second;
                it = m2.find(sendn);
                sendposy = it->second;
                it = m1.find(RSU_center_number); //center RSU x,y
                nextrsuposx = it->second;
                it = m2.find(RSU_center_number);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            //送信元から山頂RSUに送信できている場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
              //山頂RSUと自分(山頂RSUと通常RSUの距離　＋　通常RSUの通信距離100m)の通信範囲を比較
                  it = m1.find(id); //id x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(RSU_center_number); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(RSU_center_number);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
                  //printf(" b:%d ",b);
              //自分 < 山頂RSUの場合
                  if(n_hikaku_range < b && b < hikaku_range)
                  {
                //何もしない（山頂RSUがBCSTしたと予測して）
                //printf("c");
                  }
              //自分 > 山頂RSUの場合
                  else
                  {
                //BCST
                //printf("d");
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    sendn = id;
                    windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }
            //山頂RSUが受信していない場合
                }
                else
                {
              //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  sendn=id;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }
            }
          }
/*
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);        //山頂RSU以外で右半分にいるノードの場合
          if(sendn == 11)
          printf(" %d ",sendn);
*/
          else if(sendn != RSU_center_number && sendposx > 300)
          {
            //printf(" a ");
        //100m以内で受信したとみなした場合
            if(NEGATIVE_NOMAL_NODE_RANGE <=a && a <= NOMAL_NODE_RANGE)
            {
              //printf(" b ");
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id+1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id+1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
            //何もしない
              }
          //それ以外
              else
              {
            //bcst
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
                Time wait_time = Time(250.0);
                Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //B側のノードから受信したとみなした場合
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx)
        {
      //送信元が車両で自分よりB側にいる場合
          if(otemp == Nnumber-1 || otemp ==Nnumber)
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id-1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id-1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //66m以内で１つ左側のRSUが受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
          //何もしない
            }
        //それ以外で rh<=1 の場合 
            else if(rh<=1 && (otemp == Nnumber-1 || otemp ==Nnumber) )
            {
          //逆方向へのホップ
              windingHeader.SetSenderNumber (id);
              sendn_rist[id] = id;
              rs = id;
              rh=rh+1;
              windingHeader.SetHopCount (hop);  
              send_zyun_node_count = send_zyun_node_count+1;
              send_zyun_node[send_zyun_node_count] = id;
              Time wait_time = Time(250.0);
              Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
          }
        }
      }
    }
  }
}




//if Node ID = 左半分のRSU
for(i=1; i<RSU_center_number; i++)
{
  if(id == i)
  {
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
      if(1 == it->second) //発信元の車両がB方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx) //A側のノードからで受信したとみなした時
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //next B RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second; 
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE) //B方向側の１つ先のRSUが受信しリブロードしたとみなした時
          {
              //何もしない
          }
          else
          {
            //printf("rebroadcast a:%d\n",a);
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;
          } 
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //B側のノードの時
        if(sendposx > nextrsuposx)
        {
      //送信元が車両で自分よりB側にいる場合
          if(otemp == Nnumber-1 || otemp ==Nnumber)
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //送信元が真ん中より右側の場合で受信した時
            if(sendposx > 300 && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
            it = m1.find(id-1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id-1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);              
          //66m以内で１つ左側のRSUが受信したとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber-1)
              {
                it = m1.find(otemp); //sender x,y
                sendposx = it->second;
                it = m2.find(otemp);
                sendposy = it->second;
                it = m1.find(RSU_center_number); //center RSU x,y
                nextrsuposx = it->second;
                it = m2.find(RSU_center_number);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            //送信元から山頂RSUに送信できている場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
              //山頂RSUと自分(山頂RSUと通常RSUの距離　＋　通常RSUの通信距離100m)の通信範囲を比較
                  it = m1.find(id); //id x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(RSU_center_number); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(RSU_center_number);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
              //自分 < 山頂RSUの場合
                  if(n_hikaku_range < b && b < hikaku_range)
                  {
                //何もしない（山頂RSUがBCSTしたと予測して）
                  }
              //自分 > 山頂RSUの場合
                  else
                  {
                //BCST
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }
            //山頂RSUが受信していない場合
                }
                else
                {
              //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;                
                }
              }
            }
        //送信元が真ん中より左側の場合で受信した時
            else if(sendposx <= 300 && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE )
            {
          //100m以内で１つ左側のRSUが受信したとみなした時
              it = m1.find(otemp); //sender x,y
              sendposx = it->second;
              it = m2.find(otemp);
              sendposy = it->second;
              it = m1.find(id-1); //center RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                //何もしない
              }
          //それ以外で rh<=1 の場合 
              else if(rh<=1 && otemp == Nnumber-1)
              {
                it = m1.find(otemp); //sender x,y
                sendposx = it->second;
                it = m2.find(otemp);
                sendposy = it->second;
                it = m1.find(RSU_center_number); //center RSU x,y
                nextrsuposx = it->second;
                it = m2.find(RSU_center_number);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            //送信元から山頂RSUに送信できている場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
              //山頂RSUと自分(山頂RSUと通常RSUの距離　＋　通常RSUの通信距離100m)の通信範囲を比較
                  it = m1.find(id); //id x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(RSU_center_number); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(RSU_center_number);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
              //自分 < 山頂RSUの場合
                  if(n_hikaku_range < b && b < hikaku_range)
                  {
                //何もしない（山頂RSUがBCSTしたと予測して）
                  }
              //自分 > 山頂RSUの場合
                  else
                  {
                //BCST
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }
            //山頂RSUが受信していない場合
                }
                else
                {
              //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }         
            }          
          }
        }
      }
      else //発信元の車両がA方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //自分よりB側にいるノードが送信元の場合
        if(sendposx > nextrsuposx)
        {
      //山頂RSUの場合
          if(sendn == RSU_center_number)
          {
        //山頂通信距離以内で受信したとみなした場合
            if(NEGATIVE_MOUNTAIN_RSU_RANGE <=a && a <= MOUNTAIN_RSU_RANGE)
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second;
              if(HYOUKA_MOUNTAIN_RSU_RANGE == 22500)
              {
                it = m1.find(id-2); //next B RSU x,y -2
                nextrsuposx = it->second;
                it = m2.find(id-2);
                nextrsuposy = it->second;
              }
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= MOUNTAIN_RSU_RANGE)
              {
                  //何もしない
              }
          //それ以外
              else
              {
            //bcst
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
                Time wait_time = Time(250.0);
                Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }            
            }
          }
      //山頂RSU以外で右半分にいるノードの場合
          else if(sendn != RSU_center_number && 300 <= sendposx)
          {
        //66m以内で受信したとみなした場合
            if(NEGATIVE_UPHILL_RANGE <=a && a <= UPHILL_RANGE)
            {
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                  //何もしない
              }
              else
          //それ以外
              {
                it = m1.find(otemp); //sender x,y
                sendposx = it->second;
                it = m2.find(otemp);
                sendposy = it->second;
                it = m1.find(RSU_center_number); //center RSU x,y
                nextrsuposx = it->second;
                it = m2.find(RSU_center_number);
                nextrsuposy = it->second;
                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            //送信元から山頂RSUに送信できている場合
                if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
              //山頂RSUと自分(山頂RSUと通常RSUの距離　＋　通常RSUの通信距離100m)の通信範囲を比較
                  it = m1.find(id); //id x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(RSU_center_number); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(RSU_center_number);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
              //自分 < 山頂RSUの場合
                  if(n_hikaku_range < b && b < hikaku_range)
                  {
                //何もしない（山頂RSUがBCSTしたと予測して）
                  }
              //自分 > 山頂RSUの場合
                  else
                  {
                //BCST
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }
            //山頂RSUが受信していない場合
                }
                else
                {
              //bcst
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }
              }
            }
          }
      //山頂RSU以外で左半分にいるノードの場合
          else if(sendn != RSU_center_number && sendposx < 300)
          {
        //100m以内で受信したとみなした場合
            if(NEGATIVE_NOMAL_NODE_RANGE <=a && a <= NOMAL_NODE_RANGE)
            {
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              it = m1.find(sendn); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id-1); //next B RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id-1);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
          //A方向側の１つ先のRSUが受信しリブロードしたとみなした時
              if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                  //何もしない
              }
              else
              {
                //printf("rebroadcast a:%d\n",a);
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
                Time wait_time = Time(250.0);
                Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx < nextrsuposx) //A側のノードから受信したとみなした場合
        {
          if(otemp == Nnumber-1 || otemp ==Nnumber) //送信元が車両で自分よりA側にいる場合
          {
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id+1); //next A RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id+1);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        //66m以内で１つ右側のRSUが受信したとみなした時
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
                //何もしない
            }
            else if(rh<=1 && otemp == Nnumber)
            {
              windingHeader.SetSenderNumber (id);
              sendn_rist[id] = id;
              rs = id;
              rh=rh+1;
              windingHeader.SetHopCount (hop);  
              send_zyun_node_count = send_zyun_node_count+1;
              send_zyun_node[send_zyun_node_count] = id;
              Time wait_time = Time(250.0);
              Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
          }
        }
      }
    }
  }
}







  if(id == 0) /// if Node ID = 0 左端のRSU
  {
    if(vd_rist.find(otemp) == vd_rist.end() ) //ある車両から初めてパケットを受け取った時
    {
      if(0 <= windingHeader.GetOriginPositionVectorY () ) //進行方向決定
      {
        vd_rist[otemp] = 1; //vd_ristにV1の進行方向を格納
        dire = 1; //B方向に向かう車両(上から下)
        //printf("dddddddddddddddddddddddddddddddddd1 %d\n",dire);
      }
      else
      {
        vd_rist[otemp] = 0; //vd_ristにV1の進行方向を格納
        dire = 0; //A方向に向かう車両（下から上）
        //printf("dddddddddddddddddddddddddddddddddd1 %d\n",dire);
      }
    }
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      it = vd_rist.find(otemp);
  //車両がB方向の場合
      if(1 == it->second)  
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
    // A側のノード(車両)から受信したとみなした時
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx )
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //next RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
      //送信車両から自分のより1つ先のRSUが受信してるとみなした場合
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && rh<=1)
          {
            //逆方向へのホップ
            windingHeader.SetSenderNumber (id);
            sendn_rist[id] = id;
            rs = id;
            rh=1+rh;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                         
          }
      //送信車両から自分のより1つ先のRSUが受信していないとみなした場合
          else
          {
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            rh=1+rh; //不要なリブロードキャストをなくすため
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                                       
          }
        }
        it = m1.find(Nnumber-1); //sender x,y
        sendposx = it->second;
        it = m2.find(Nnumber-1);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
    //送信車両がB側にいて受信している場合
        if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx > nextrsuposx && rh<=1) //&& sendn==Nnumber-1)
        {
          //逆方向へのホップ
          windingHeader.SetSenderNumber (id);
          sendn_rist[id] = id;
          rs = id;
          rh=1+rh;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();
          packettemp[pnm][id] = 1;  
        }
      }
      else //車両の方向がA方向の時
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU(0) x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
        (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(sendposx >= nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE) //自分よりB側にいる送信者から受信したとみなした時
        {
          windingHeader.SetSenderNumber (id);
          sendn = id;
          sendn_rist[id] = id;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();  
          packettemp[pnm][id] = 1;
        }
        else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE ) //自分よりA側にいる送信者から受信したとみなした時
        { 
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id+1); //my RSU(0) x,y
          nextrsuposx = it->second;
          it = m2.find(id+1);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          if(NEGATIVE_UPHILL_RANGE <= a && a<= UPHILL_RANGE)
          {
          }
          else if(rh <= 1) //逆方向へのホップ
          {
            windingHeader.SetSenderNumber (id); 
            sendn_rist[id] = id;
            rs = id;
            rh = rh+1;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
            hoptemp[pnm][id] = windingHeader.GetHopCount ();  
            packettemp[pnm][id] = 1;
          }
        }
      }
    }
  }




//// 右端のRSU
  if(id == Nnumber-10)
  {
    if(vd_rist.find(otemp) == vd_rist.end() ) //ある車両から初めてパケットを受け取った時
    {
      if(0 >= windingHeader.GetOriginPositionVectorY () ) //進行方向決定 
      {                           
        vd_rist[otemp] = 0; //vd_ristにV1の進行方向を格納
        dire = 0; //A方向に向かう車両（下から上）
        //printf("dddddddddddddddddddddddddddddddddd2 %d\n",dire);
      }
      else
      {
        vd_rist[otemp] = 1; //vd_ristにV1の進行方向を格納
        dire = 1; //B方向に向かう車両(上から下)
        //printf("dddddddddddddddddddddddddddddddddd2 %d\n",dire);
      }
    }
    if(1 != packettemp[pnm][id])
    {
      hoptemp[pnm][id] = windingHeader.GetHopCount (); //格納忘れ防止
      //printf("hoptemp[%d][%d] = %d\n",pnm,i,hop);
      it = vd_rist.find(otemp);
      if(1 == it->second) //車両がB方向のとき
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU(0) x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(sendposx <= nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE) //自分よりA側にいる送信者から受信したとみなした時
        {
          windingHeader.SetSenderNumber (id);
          sendn = id;
          sendn_rist[id] = id;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();  
          packettemp[pnm][id] = 1;
        }
        if(sendposx > nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE ) //自分よりB側にいる送信者から受信したとみなした時
        { 
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //my RSU(0) x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d \n",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          if(NEGATIVE_UPHILL_RANGE <= a && a<= UPHILL_RANGE)
          {
          }
          else if(rh <= 1) //逆方向へのホップ
          {
            windingHeader.SetSenderNumber (id); 
            sendn_rist[id] = id;
            rs = id;
            rh = rh+1;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();  
            packettemp[pnm][id] = 1;
          }
        }
      }
      else //車両がA方向のとき
      {
        it = m1.find(sendn); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx ) // B側のノード(車両)から受信したとみなした時
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id-1); //next RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id-1);
          nextrsuposy = it->second;
          //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && rh<=1) //送信車両から自分のより1つ先のRSUが受信してるとみなした場合
          {
            //逆方向へのホップ
            windingHeader.SetSenderNumber (id);
            sendn_rist[id] = id;
            rs = id;
            rh=1+rh;
            hop = hop+1;
            windingHeader.SetHopCount (hop);
             send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                                      
          }
          else  ////送信車両から自分のより1つ先のRSUが受信していないとみなした場合
          {
            windingHeader.SetSenderNumber (id);
            sendn = id;
            sendn_rist[id] = id;
            hop = hop+1;
            rh=1+rh; //不要なリブロードキャストをなくすため
            windingHeader.SetHopCount (hop);
            send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
            hoptemp[pnm][id] = windingHeader.GetHopCount ();
            packettemp[pnm][id] = 1;                                       
          }
        }
        it = m1.find(otemp); //sender x,y
        sendposx = it->second;
        it = m2.find(otemp);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && sendposx < nextrsuposx && rh<=1 && otemp==Nnumber)
        {
          windingHeader.SetSenderNumber (id);
          sendn_rist[id] = id;
          rs = id;
          rh=1+rh;
          hop = hop+1;
          windingHeader.SetHopCount (hop);
           send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);

          hoptemp[pnm][id] = windingHeader.GetHopCount ();
          packettemp[pnm][id] = 1;
        }                        
      }
    }
  }




//if Node ID = 山頂RSU
    if(id == RSU_center_number)
    {
      if(1 != packettemp[pnm][id])
      {
        hoptemp[pnm][id] = windingHeader.GetHopCount (); //hop count格納忘れ防止
        it = vd_rist.find(otemp); //車両の進行方向
  //発信元の車両がB方向の時
        if(1 == it->second)
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //自分よりA側のノードからで受信したとみなした時
          if( NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx <= nextrsuposx)
          {
      //送信元の通信範囲内でパケット進行方向先に最も遠いRSUを判断（farthest_rsu）
            farthest_rsu = RSU_center_number; //初期化
            next_rsu_count = 0; //初期化
            while (NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
              next_rsu_count = next_rsu_count+1; //更に先のRSUへ
              it = m1.find(sendn); //sender送信元 x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id + next_rsu_count); //next B側 RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id + next_rsu_count);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            } 
            farthest_rsu = id + next_rsu_count -1; //受信した最も遠いRSU番号を格納
      //先のRSUが受信していない場合
            if (farthest_rsu == RSU_center_number && 1 != packettemp[pnm][id])
            {
              windingHeader.SetSenderNumber (id);
              sendn = id;
              sendn_rist[id] = id;
              hop = hop+1;
              windingHeader.SetHopCount (hop);
              send_zyun_node_count = send_zyun_node_count+1;
              send_zyun_node[send_zyun_node_count] = id;
              Time wait_time = Time(250.0);
              Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
            else //いる場合→山頂RSUと遠いRSUの通信範囲を比較
            {
              it = m1.find(id); //my x,y
              sendposx = it->second;
              it = m2.find(id);
              sendposy = it->second;
              it = m1.find(farthest_rsu); //center RSU x,y
              nextrsuposx = it->second;
              it = m2.find(farthest_rsu);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
              b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
              if(b > hikaku_range) //Nnumber(b) > 山頂RSUの場合は何もしない（山頂RSUがBCSTしたと予測して）
              {
                //Nnumber > 山頂RSUの場合は何もしない（山頂RSUがBCSTしたと予測して）
              }
              else if(1 != packettemp[pnm][id])  //Nnumber < 山頂RSUの場合は山頂RSUがBCST
              {
                //Nnumber < 山頂RSUの場合は山頂RSUがBCST
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
                send_zyun_node[send_zyun_node_count] = id;
                Time wait_time = Time(250.0);
                Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }
            }
          }
          ///後方車両向けの送信////
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx > nextrsuposx) //B側のノードから受信したとみなした場合
            {
              if(otemp == Nnumber-1 || otemp ==Nnumber) //送信元が車両で自分よりB側にいる場合
              {
        //送信元の通信範囲内でパケット進行方向先に最も遠いRSUを判断（farthest_rsu）
            farthest_rsu = RSU_center_number; //初期化
            next_rsu_count = 0; //初期化
            while (NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
              next_rsu_count = next_rsu_count+1; //更に先のRSUへ
              it = m1.find(sendn); //sender送信元 x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id - next_rsu_count); //next B側 RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id - next_rsu_count);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            } 
            farthest_rsu = id - next_rsu_count +1; //受信した最も遠いRSU番号を格納
            //printf("rsu count : %d    ",farthest_rsu); //受信した最も遠いRSU判別の確認用
        //先のRSUが受信していない場合
                if (farthest_rsu == RSU_center_number && rh<=1 && otemp == Nnumber-1) 
                {
          //山頂RSUがBCST
                  windingHeader.SetSenderNumber (i);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                  send_zyun_node_count = send_zyun_node_count+1;
                  send_zyun_node[send_zyun_node_count] = id;
                  Time wait_time = Time(250.0);
                  Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }  
        //いる場合→山頂RSUと遠いRSUの通信範囲を比較
                else if(farthest_rsu != RSU_center_number && rh<=1 && otemp == Nnumber-1) 
                  it = m1.find(id); //my x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(farthest_rsu); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(farthest_rsu);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
                  if(b > hikaku_range) //Nnumber(b) > 山頂RSUの場合は何もしない（山頂RSUがBCSTしたと予測して）
                  {
                    //Nnumber > 山頂RSUの場合は何もしない（山頂RSUがBCSTしたと予測して）
                  }
                  else  //Nnumber < 山頂RSUの場合は山頂RSUがBCST
                  {
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);    
                    send_zyun_node_count = send_zyun_node_count+1;
                    send_zyun_node[send_zyun_node_count] = id;
                    Time wait_time = Time(250.0);
                    Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }             
                }
              }
        } 
  //発信元の車両がA方向の時
        else
        {
          it = m1.find(sendn); //sender x,y
          sendposx = it->second;
          it = m2.find(sendn);
          sendposy = it->second;
          it = m1.find(id); //my RSU x,y
          nextrsuposx = it->second;
          it = m2.find(id);
          nextrsuposy = it->second;
          a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //B側のノードからで受信したとみなした時
          if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx >= nextrsuposx)
          {
      //送信元の通信範囲内でパケット進行方向先に最も遠いRSUを判断（farthest_rsu）
            farthest_rsu = RSU_center_number; //初期化
            next_rsu_count = 0; //初期化
            while (NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
              next_rsu_count = next_rsu_count+1; //更に先のRSUへ
              it = m1.find(sendn); //sender送信元 x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id - next_rsu_count); //next B側 RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id - next_rsu_count);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            } 
            farthest_rsu = id - next_rsu_count +1; //受信した最も遠いRSU番号を格納
      //先のRSUが受信していない場合
            if (farthest_rsu == RSU_center_number) 
            {
        //山頂RSUがBCST
              windingHeader.SetSenderNumber (id);
              sendn = id;
              sendn_rist[id] = id;
              hop = hop+1;
              windingHeader.SetHopCount (hop);
                send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
              hoptemp[pnm][id] = windingHeader.GetHopCount ();
              packettemp[pnm][id] = 1;
            }
      //いる場合→山頂RSUと遠いRSUの通信範囲を比較
            else
            {
              it = m1.find(id); //my x,y
              sendposx = it->second;
              it = m2.find(id);
              sendposy = it->second;
              it = m1.find(farthest_rsu); //center RSU x,y
              nextrsuposx = it->second;
              it = m2.find(farthest_rsu);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
              b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
        //Nnumber(b) > 山頂RSUの場合
              if(b > hikaku_range)
              {
          //何もしない（山頂RSUがBCSTしたと予測して）
              }
        //Nnumber < 山頂RSUの場合
              else
              {
          //山頂RSUがBCST
                windingHeader.SetSenderNumber (id);
                sendn = id;
                sendn_rist[id] = id;
                hop = hop+1;
                windingHeader.SetHopCount (hop);
                  send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                hoptemp[pnm][id] = windingHeader.GetHopCount ();
                packettemp[pnm][id] = 1;
              }             
            }
          }
            // 後方車両向け //
            it = m1.find(otemp); //sender x,y
            sendposx = it->second;
            it = m2.find(otemp);
            sendposy = it->second;
            it = m1.find(id); //my RSU x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +(nextrsuposx - sendposx)*(nextrsuposx - sendposx);
    //A側のノードから受信したとみなした場合
            if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && sendposx < nextrsuposx)
            {
      //送信元が車両
              if(otemp == Nnumber-1 || otemp ==Nnumber)
              {
        //送信元の通信範囲内でパケット進行方向先に最も遠いRSUを判断（farthest_rsu）
            farthest_rsu = RSU_center_number; //初期化
            next_rsu_count = 0; //初期化
            while (NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
            {
              next_rsu_count = next_rsu_count+1; //更に先のRSUへ
              it = m1.find(sendn); //sender送信元 x,y
              sendposx = it->second;
              it = m2.find(sendn);
              sendposy = it->second;
              it = m1.find(id + next_rsu_count); //next B側 RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id + next_rsu_count);
              nextrsuposy = it->second; 
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
            } 
            farthest_rsu = id + next_rsu_count -1; //受信した最も遠いRSU番号を格納
        //先のRSUが受信していない場合
                if (farthest_rsu == RSU_center_number && rh<=1 && otemp == Nnumber)
                {
          //山頂RSUがBCST
                  windingHeader.SetSenderNumber (id);
                  sendn_rist[id] = id;
                  rs = id;
                  rh=rh+1;
                  windingHeader.SetHopCount (hop);  
                    send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                  hoptemp[pnm][id] = windingHeader.GetHopCount ();
                  packettemp[pnm][id] = 1;
                }  
        //いる場合→山頂RSUと遠いRSUの通信範囲を比較
                else if(farthest_rsu != RSU_center_number && rh<=1 && otemp == Nnumber)
                {
                  it = m1.find(id); //my x,y
                  sendposx = it->second;
                  it = m2.find(id);
                  sendposy = it->second;
                  it = m1.find(farthest_rsu); //center RSU x,y
                  nextrsuposx = it->second;
                  it = m2.find(farthest_rsu);
                  nextrsuposy = it->second;
                  a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
                  b = a + NOMAL_NODE_RANGE; //b = 山頂RSUと通常RSUの距離　＋　通常RSUの通信距離
          //Nnumber(b) > 山頂RSUの場合
                  if(b > MOUNTAIN_RSU_RANGE)
                  {
            //何もしない（山頂RSUがBCSTしたと予測して）
                  }
          //Nnumber < 山頂RSUの場合
                  else
                  {
            //山頂RSUがBCST
                    windingHeader.SetSenderNumber (id);
                    sendn_rist[id] = id;
                    rs = id;
                    rh=rh+1;
                    windingHeader.SetHopCount (hop);  
                      send_zyun_node_count = send_zyun_node_count+1;
            send_zyun_node[send_zyun_node_count] = id;
            Time wait_time = Time(250.0);
            Simulator::Schedule(wait_time, &RoutingProtocol::SendWBroadcast, this);
                    hoptemp[pnm][id] = windingHeader.GetHopCount ();
                    packettemp[pnm][id] = 1;
                  }             
                }
              }
            }
        }
      }
    }






/*********************************** 送信者下り 対向車の評価 *******************************************/
if(HYOUKA == 1)
{
  for(i=0;i<=send_end_count;i++)
  {
    if(send_zyun_node[i] != 100)
    {
      if(id == Nnumber) //destination node
      {
                if(1 != packettemp[pnm][id])
                {
                        GetSourcePosition();
                        it = m1.find(Nnumber-1); //source車両 x,y
                        int sendposx = it->second;
                        it = m2.find(Nnumber-1);
                        int sendposy = it->second;
                        it = m1.find(id); //destination x,y
                        int nextrsuposx = it->second;
                        it = m2.find(id);
                        int nextrsuposy = it->second;
                        int b = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx); //source car no tuusinhanni ni hairumade
                        if(nextrsuposx > sendposx && 500 > sendposx && sendposx >= HYOKA_KUKAN && b >=HYOUKA_END_DISTANCE && otemp == Nnumber-1 ) //source車両とdestinationがすれ違うまで
                        //if(100 < nextrsuposx && nextrsuposx < sendposx && otemp == Nnumber-1 && b >=10000) //source車両とdestinationがすれ違うまで
                        {

                                it = m1.find(send_zyun_node[i]); //sender x,y
                                sendposx = it->second;
                                it = m2.find(send_zyun_node[i]);
                                sendposy = it->second;
                                it = m1.find(id); //my  x,y
                                nextrsuposx = it->second;
                                it = m2.find(id);
                                nextrsuposy = it->second;
                                a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                                    (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
                                    if(send_zyun_node[i]==RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
                                    {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id] << " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                    }
                                   if(send_zyun_node[i] != RSU_center_number && nextrsuposx <= sendposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id]; 
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
                                   }
                                   if(send_zyun_node[i] != RSU_center_number &&  nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE )
                                   {
                          evaluation_start = 1; //評価スタートフラグ
                          Time receive_time = Simulator::Now (); //パケット受信時刻
                          receive_count[id]++; 
                          Time wait_time = Time(250.0);
                          Time total_wait_time = wait_time * send_end_count;
                          Time one_delay_time = receive_time - vehicle_send_time - total_wait_time;
                          total_delay_time[id] = total_delay_time[id] + one_delay_time;
                          average_delay_time[id] = total_delay_time[id] / receive_count[id];
                          packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                          total_hop = total_hop + send_end_count;
                          heikin_receive_hop = total_hop / receive_count[id];
                          std::cout<<"  "<< receive_time << "  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " "<< send_zyun_node[i]<< " "<<send_end_count<<" "<<total_hop<<" "<<heikin_receive_hop;
                          packettemp[pnm][id] = 1;
//printf("%d",sendn_rist[j]);
                                   }

                        }
                }  
      }
    }
  }
}
/*************************************** パケット受信数のカウント ************************************/

int n=0;
  for(i = Nnumber-9; i <= Nnumber; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
        for(n=0; n<=50; n++)
        {
          if(sendn_rist[n] != 100)
          {
  //受け取ったパケットの送信元リストにのっていない場合（車両のパケット送信毎にリストを初期化している）
            if(recv_rist[id][sendn_rist[n]] != 1)
            {
              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元ノードが山頂RSUの場合はそのままの通信範囲で受信成功とみなす
    	        if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //受信車両の位置が真ん中より右側の場合
              else if(300 < nextrsuposx)
              {
    //送信元が自分より左側で真ん中より右側の場合は距離が100m以内だと通信成功とみなす
                if(300 < sendposx && sendposx < nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
  //受信車両の位置が真ん中より左側の場合
              else if(300 > nextrsuposx)
              {
    //送信元が自分より右側で真ん中より左側の場合は距離が100m以内だと通信成功とみなす
                if(300 > sendposx && sendposx > nextrsuposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
    //それ以外の場合は距離が66以内だと通信成功とみなす
                else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
                {
                  packetcnt++;
                  recv_rist[id][sendn_rist[n]] = 1;
                }
              }
            }
          }
        }
      }
    }
  }



//左側のRSUノードである
  for(i = 0; i <= RSU_center_number-1; i++)
  {
    if(id == i)
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より左側の場合はお互いの距離が６６m以内だと受信成功とみなす
              else if(sendposx < nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードかつ、自分より右側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx < 300 && nextrsuposx < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 < sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
      }
    }
  }

//真ん中のRSUノードである
  if(id == RSU_center_number)
  {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
    for(n=0; n<=50; n++)
    {
		  if(sendn_rist[n] != 100)
		  {
      if(recv_rist[id][sendn_rist[n]] != 1)
{        
  //送信元の全ノードが66m以内だと受信成功とみなす→bcst
        it = m1.find(sendn_rist[n]); //sender x,y
        sendposx = it->second;
        it = m2.find(sendn_rist[n]);
        sendposy = it->second;
        it = m1.find(id); //my RSU x,y
        nextrsuposx = it->second;
        it = m2.find(id);
        nextrsuposy = it->second;
        //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
        a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
        if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
        {
          packetcnt++;
          recv_rist[id][sendn_rist[n]] = 1;
        }
      }
    }
    }
  }
  }

//右側のRSUノードである
  for(i = RSU_center_number+1; i <= Nnumber-10; i++)
  {
    if(id == i )
    {
      GetSourcePosition();
      it = m1.find(Nnumber-1); //right車両 x
      int sendposx = it->second;
      if( 100 <= sendposx && sendposx <500) //先頭車両がカーブ区間にいる時のパケット受信数を取得
      {
      for(n=0; n<=50; n++)
      {
		    if(sendn_rist[n] != 100)
		    {
if(recv_rist[id][sendn_rist[n]] != 1){

              it = m1.find(sendn_rist[n]); //sender x,y
              sendposx = it->second;
              it = m2.find(sendn_rist[n]);
              sendposy = it->second;
              it = m1.find(id); //my RSU x,y
              nextrsuposx = it->second;
              it = m2.find(id);
              nextrsuposy = it->second;
              //printf("sendx:%d sendy:%d nextx:%d nexty%d ",sendposx,sendposy,nextrsuposx,nextrsuposy);
              a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) + (nextrsuposx - sendposx)*(nextrsuposx - sendposx);
  //送信元が真ん中のノードの場合はそのままの通信範囲で受信成功とする→bcst
              if(sendn_rist[n] == RSU_center_number && HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が自分より右側の場合はお互いの距離が６６m以内だと受信成功とみなす→bcst
              else if(sendposx > nextrsuposx && NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より右側のノードかつ、自分より左側の場合はお互いの距離が100m以内だと受信成功とみなす→bcst
              else if(sendposx > 300 && nextrsuposx > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }
  //送信元が真ん中より左側のノードの場合は距離が66ｍ以内だと受信成功とみなす→bcst
              else if(300 > sendposx && NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE)
              {
                packetcnt++;
                recv_rist[id][sendn_rist[n]] = 1;
              }  
        }
        }
      }
    }
    }
  }

/**************************************下り車両から後方車両へ********************************************/
if(HYOUKA == 2)
{
  int j;
  for(j=0;j<=send_end_count;j++)
  {
    if(send_zyun_node[j] != 100)
    {
      if(Nnumber-9 <= id && id <= Nnumber-6)
      {
        if(1 != packettemp_t[pnm][id])
        {
          GetDestPosition();
          it = m1.find(Nnumber-1); //source車両 x,y
          sendposx = it->second;
          it = m2.find(Nnumber-1);
          sendposy = it->second;
          if(500 > sendposx && sendposx >= 100 && otemp == Nnumber-1) //source車両が峠道区間をぬけるまで
          {
            it = m1.find(send_zyun_node[j]); //source車両 x,y
            sendposx = it->second;
            it = m2.find(send_zyun_node[j]);
            sendposy = it->second;
            it = m1.find(id); //my  x,y
            nextrsuposx = it->second;
            it = m2.find(id);
            nextrsuposy = it->second;
            a = (nextrsuposy - sendposy)*(nextrsuposy - sendposy) +
                (nextrsuposx - sendposx)*(nextrsuposx - sendposx);  
//送信ノードが真ん中にいる（送信車両が山頂RSUノード）
            if(send_zyun_node[j] == RSU_center_number)
            {
  //山頂RSUの通信距離内なら受信成功
              if(HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE <= a && a <= HYOUKA_MOUNTAIN_RSU_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
//送信ノードが左側にいる（送信車両も含む）
            else if(sendposx < 300)
            {
  //送信ノードより受信ノードの方が左側の場合
              if(nextrsuposx < sendposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id] )
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                  packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              } 
            }
//送信ノードが右にいる（送信車両も含む）
            else if(300 < sendposx)
            {
  //送信ノードより受信ノードの方が右側の場合
              if(sendposx < nextrsuposx)
              {
    //100m以内で受信成功
                if(NEGATIVE_NOMAL_NODE_RANGE <= a && a <= NOMAL_NODE_RANGE && 1 != packettemp_t[pnm][id])
                {
                  evaluation_start = 1; //評価スタートフラグ
                  Time receive_time = Simulator::Now (); //パケット受信時刻
                  receive_count[id]++; 
                  Time one_delay_time = receive_time - vehicle_send_time;
                  total_delay_time[id] = total_delay_time[id] + one_delay_time;
                  average_delay_time[id] = total_delay_time[id] / receive_count[id];
                  packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                  std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                 //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                 packettemp_t[pnm][id] = 1;
                }
  //それ以外は66m以内で受信成功     
              }
              else if(NEGATIVE_UPHILL_RANGE <= a && a <= UPHILL_RANGE && 1 != packettemp_t[pnm][id])
              {
                evaluation_start = 1; //評価スタートフラグ
                Time receive_time = Simulator::Now (); //パケット受信時刻
                receive_count[id]++; 
                Time one_delay_time = receive_time - vehicle_send_time;
                total_delay_time[id] = total_delay_time[id] + one_delay_time;
                average_delay_time[id] = total_delay_time[id] / receive_count[id];
                packet_totatu[id] = receive_count[id] / evaluation_send_count * 100.0;
                std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id];//<< " " << send_zyun_node[j] << " " << send_end_count;
                //std::cout<<"  "<<average_delay_time[id] << "  "<<packet_totatu[id]<< " " << send_zyun_node[j] << " " << send_end_count;
                packettemp_t[pnm][id] = 1;
              }
            }
          }
        }
      }
    }
  }
}



} //winding3

} //namespace shide
} //namespace ns3

