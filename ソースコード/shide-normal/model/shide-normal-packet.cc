/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 Takumu Shide
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Takumu Shide <is0310xi@ed.ritsumei.ac.jp>
 *         Ritsumeikan University, Shiga, Japan
 */
#include "shide-normal-packet.h"
#include "ns3/address-utils.h"
#include "ns3/packet.h"

namespace ns3 {
namespace shideNormal {

NS_OBJECT_ENSURE_REGISTERED (TypeHeader);

TypeHeader::TypeHeader (MessageType t)
  : m_type (t),
    m_valid (true)
{
}

TypeId
TypeHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::shideNormal::TypeHeader")
    .SetParent<Header> ()
    .SetGroupName ("ShideNormal")
    .AddConstructor<TypeHeader> ()
  ;
  return tid;
}

TypeId
TypeHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
TypeHeader::GetSerializedSize () const
{
  return 1;
}

void
TypeHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 ((uint8_t) m_type);
}

uint32_t
TypeHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t type = i.ReadU8 ();
  m_valid = true;
  switch (type)
    {
    case SHIDETYPE_RREQ:
    case SHIDETYPE_RREP:
    case SHIDETYPE_RERR:
    case SHIDETYPE_RREP_ACK:
    case SHIDETYPE_WINDING:
      {
        m_type = (MessageType) type;
        break;
      }
    default:
      m_valid = false;
    }
  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
TypeHeader::Print (std::ostream &os) const
{
  switch (m_type)
    {
    case SHIDETYPE_RREQ:
      {
        os << "RREQ";
        break;
      }
    case SHIDETYPE_RREP:
      {
        os << "RREP";
        break;
      }
    case SHIDETYPE_RERR:
      {
        os << "RERR";
        break;
      }
    case SHIDETYPE_RREP_ACK:
      {
        os << "RREP_ACK";
        break;
      }
    case SHIDETYPE_WINDING:
      {
        os << "WINDING";
        break;
      }
    default:
      os << "UNKNOWN_TYPE";
    }
}

bool
TypeHeader::operator== (TypeHeader const & o) const
{
  return (m_type == o.m_type && m_valid == o.m_valid);
}

std::ostream &
operator<< (std::ostream & os, TypeHeader const & h)
{
  h.Print (os);
  return os;
}

//---------------------------------------------------------------------------
// WINDING
//---------------------------------------------------------------------------


/*
WindingHeader::WindingHeader (uint8_t hopCount,
                                uint8_t direction,
                                uint8_t flag,
                                uint32_t sourcePosition, 
                                Ipv4Address sourceIp,
                                Time lifeTime,
                                uint32_t free)
  : m_hopCount(hopCount),
    m_direction(direction),
    m_flag(flag),
    m_sourcePosition(sourcePosition),
    m_sourceIp(sourceIp),
    m_free(free)

{
  m_lifeTime = uint32_t (lifeTime.GetMilliSeconds ());
}
*/


/*
NS_OBJECT_ENSURE_REGISTERED (WindingHeader);


TypeId
WindingHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::shide::WindingHeader")
    .SetParent<Header> ()
    .SetGroupName ("Shide")
    .AddConstructor<WindingHeader> ()
  ;
  return tid;
}
*/

WindingHeader::WindingHeader (uint8_t packetNumber, uint8_t direction, uint8_t hopCount, uint16_t originPositionX,uint16_t originPositionY,
                              uint16_t originPositionVectorX,uint16_t originPositionVectorY, Ipv4Address origin,uint16_t senderNumber, Time lifeTime, uint8_t flag)

  : m_flags (0),
    m_packetNumber (packetNumber),
    m_direction (direction),
    m_hopCount (hopCount),
    m_originPositionX (originPositionX),
    m_originPositionY (originPositionY),
    m_originPositionVectorX (originPositionVectorX),
    m_originPositionVectorY (originPositionVectorY),
    m_origin (origin),
    m_senderNumber (senderNumber),
    m_flag (flag)
{
  m_lifeTime = uint16_t (lifeTime.GetMilliSeconds ());
}

NS_OBJECT_ENSURE_REGISTERED (WindingHeader);

TypeId
WindingHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::shideNormal::WindingHeader")
    .SetParent<Header> ()
    .SetGroupName ("ShideNormal")
    .AddConstructor<WindingHeader> ()
  ;
  return tid;
}

TypeId
WindingHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
WindingHeader::GetSerializedSize () const
{
  return 21;
}

void
WindingHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_flags);
  i.WriteU8 (m_packetNumber);
  i.WriteU8 (m_direction);
  i.WriteU8 (m_hopCount);
  i.WriteHtonU16 (m_originPositionX);
  i.WriteHtonU16 (m_originPositionY);
  i.WriteHtonU16 (m_originPositionVectorX);
  i.WriteHtonU16 (m_originPositionVectorY);
  WriteTo (i, m_origin);
  i.WriteHtonU16 (m_senderNumber);
  i.WriteHtonU16 (m_lifeTime);
  i.WriteU8 (m_flag);
  
}

uint32_t
WindingHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  m_flags = i.ReadU8 ();
  m_packetNumber = i.ReadU8 ();
  m_direction = i.ReadU8 ();
  m_hopCount = i.ReadU8 ();
  m_originPositionX = i.ReadNtohU16 ();
  m_originPositionY = i.ReadNtohU16 ();
  m_originPositionVectorX = i.ReadNtohU16 ();
  m_originPositionVectorY = i.ReadNtohU16 ();
  ReadFrom (i, m_origin);
  m_senderNumber = i.ReadNtohU16 ();
  m_lifeTime = i.ReadNtohU16 ();
  m_flag = i.ReadU8 ();

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
WindingHeader::Print (std::ostream &os) const
{
  os << /*"destination: ipv4 " << m_dst <<*/
         " originPosition number " << m_originPositionX;
  /*if (m_direction != 0)
    {
      os << " direction " << m_direction;
    }*/
  os << " source ipv4 " << m_origin << " lifetime " << m_lifeTime
     << " acknowledgment required flag " << (*this).GetAckRequired ();
}

void
WindingHeader::SetLifeTime (Time t)
{
  m_lifeTime = t.GetMilliSeconds ();
}

Time
WindingHeader::GetLifeTime () const
{
  Time t (MilliSeconds (m_lifeTime));
  return t;
}

void
WindingHeader::SetAckRequired (bool f)
{
  if (f)
    {
      m_flags |= (1 << 6);
    }
  else
    {
      m_flags &= ~(1 << 6);
    }
}

bool
WindingHeader::GetAckRequired () const
{
  return (m_flags & (1 << 6));
}

/*
void
WindingHeader::SetDirection (uint8_t sz)
{
  m_direction = sz;
}

uint8_t
WindingHeader::GetDirection () const
{
  return m_direction;
}
*/


bool
WindingHeader::operator== (WindingHeader const & o) const
{
  return (m_flags == o.m_flags && m_packetNumber == o.m_packetNumber && m_direction == o.m_direction
          && m_hopCount == o.m_hopCount && m_originPositionX == o.m_originPositionX && m_originPositionY == o.m_originPositionY && m_originPositionVectorX == o.m_originPositionVectorX && m_originPositionVectorY == o.m_originPositionVectorY
          && m_origin == o.m_origin && m_senderNumber == o.m_senderNumber && m_lifeTime == o.m_lifeTime && m_flag == o.m_flag);
}

void
WindingHeader::SetHello (Ipv4Address origin, uint32_t srcSeqNo, Time lifetime)
{
  m_flags = 0;
  m_packetNumber = 0;
  m_direction = 0;
  m_hopCount = 0;
  m_originPositionX = 0;
  m_originPositionY = 0;
  m_originPositionVectorX = 0;
  m_originPositionVectorY = 0;
  m_origin = origin;
  m_senderNumber =  0;
  m_lifeTime = lifetime.GetMilliSeconds ();
  m_flag = 0;
}

std::ostream &
operator<< (std::ostream & os, WindingHeader const & h)
{
  h.Print (os);
  return os;
}

//-----------------------------------------------------------------------------
// RREP
//-----------------------------------------------------------------------------

RrepHeader::RrepHeader (uint8_t prefixSize, uint8_t hopCount, Ipv4Address dst,
                        uint32_t dstSeqNo, Ipv4Address origin, Time lifeTime)
  : m_flags (0),
    m_prefixSize (prefixSize),
    m_hopCount (hopCount),
    m_dst (dst),
    m_dstSeqNo (dstSeqNo),
    m_origin (origin)
{
  m_lifeTime = uint32_t (lifeTime.GetMilliSeconds ());
}

NS_OBJECT_ENSURE_REGISTERED (RrepHeader);

TypeId
RrepHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::shideNormal::RrepHeader")
    .SetParent<Header> ()
    .SetGroupName ("ShideNormal")
    .AddConstructor<RrepHeader> ()
  ;
  return tid;
}

TypeId
RrepHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
RrepHeader::GetSerializedSize () const
{
  return 19;
}

void
RrepHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_flags);
  i.WriteU8 (m_prefixSize);
  i.WriteU8 (m_hopCount);
  WriteTo (i, m_dst);
  i.WriteHtonU32 (m_dstSeqNo);
  WriteTo (i, m_origin);
  i.WriteHtonU32 (m_lifeTime);
}

uint32_t
RrepHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  m_flags = i.ReadU8 ();
  m_prefixSize = i.ReadU8 ();
  m_hopCount = i.ReadU8 ();
  ReadFrom (i, m_dst);
  m_dstSeqNo = i.ReadNtohU32 ();
  ReadFrom (i, m_origin);
  m_lifeTime = i.ReadNtohU32 ();

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
RrepHeader::Print (std::ostream &os) const
{
  os << "destination: ipv4 " << m_dst << " sequence number " << m_dstSeqNo;
  if (m_prefixSize != 0)
    {
      os << " prefix size " << m_prefixSize;
    }
  os << " source ipv4 " << m_origin << " lifetime " << m_lifeTime
     << " acknowledgment required flag " << (*this).GetAckRequired ();
}

void
RrepHeader::SetLifeTime (Time t)
{
  m_lifeTime = t.GetMilliSeconds ();
}

Time
RrepHeader::GetLifeTime () const
{
  Time t (MilliSeconds (m_lifeTime));
  return t;
}

void
RrepHeader::SetAckRequired (bool f)
{
  if (f)
    {
      m_flags |= (1 << 6);
    }
  else
    {
      m_flags &= ~(1 << 6);
    }
}

bool
RrepHeader::GetAckRequired () const
{
  return (m_flags & (1 << 6));
}

void
RrepHeader::SetPrefixSize (uint8_t sz)
{
  m_prefixSize = sz;
}

uint8_t
RrepHeader::GetPrefixSize () const
{
  return m_prefixSize;
}

bool
RrepHeader::operator== (RrepHeader const & o) const
{
  return (m_flags == o.m_flags && m_prefixSize == o.m_prefixSize
          && m_hopCount == o.m_hopCount && m_dst == o.m_dst && m_dstSeqNo == o.m_dstSeqNo
          && m_origin == o.m_origin && m_lifeTime == o.m_lifeTime);
}

void
RrepHeader::SetHello (Ipv4Address origin, uint32_t srcSeqNo, Time lifetime)
{
  m_flags = 0;
  m_prefixSize = 0;
  m_hopCount = 0;
  m_dst = origin;
  m_dstSeqNo = srcSeqNo;
  m_origin = origin;
  m_lifeTime = lifetime.GetMilliSeconds ();
}

std::ostream &
operator<< (std::ostream & os, RrepHeader const & h)
{
  h.Print (os);
  return os;
}

}
}
