/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 */
#ifndef SHIDEROUTINGPROTOCOL_H
#define SHIDEROUTINGPROTOCOL_H


#include "shide-normal-packet.h"
#include "ns3/node.h"
#include "ns3/random-variable-stream.h"
#include "ns3/output-stream-wrapper.h"
#include "ns3/ipv4-routing-protocol.h"
#include "ns3/ipv4-interface.h"
#include "ns3/ipv4-l3-protocol.h"
#include <map>

#include "ns3/mobility-module.h"

namespace ns3 {
namespace shideNormal {
/**
 * \ingroup shide
 *
 * \brief SHIDE routing protocol
 */
class RoutingProtocol : public Ipv4RoutingProtocol
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  static const uint32_t SHIDE_PORT;

  /// constructor
  RoutingProtocol ();
  virtual ~RoutingProtocol ();
  virtual void DoDispose ();

  // Inherited from Ipv4RoutingProtocol  Ipv4RoutingProtocolから継承される。
  Ptr<Ipv4Route> RouteOutput (Ptr<Packet> p, const Ipv4Header &header, Ptr<NetDevice> oif, Socket::SocketErrno &sockerr);
  bool RouteInput (Ptr<const Packet> p, const Ipv4Header &header, Ptr<const NetDevice> idev,
                   UnicastForwardCallback ucb, MulticastForwardCallback mcb,
                   LocalDeliverCallback lcb, ErrorCallback ecb);
  virtual void NotifyInterfaceUp (uint32_t interface);
  virtual void NotifyInterfaceDown (uint32_t interface);
  virtual void NotifyAddAddress (uint32_t interface, Ipv4InterfaceAddress address);
  virtual void NotifyRemoveAddress (uint32_t interface, Ipv4InterfaceAddress address);
  virtual void SetIpv4 (Ptr<Ipv4> ipv4);
  virtual void PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit = Time::S) const;

/*
  void SendTo (Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination);

*/

  /**
   * Assign a fixed random variable stream number to the random variables
   * used by this model.  Return the number of streams (possibly zero) that
   * have been assigned.
   *
   * \param stream first stream index to use
   * \return the number of stream indices assigned by this model
   */
  int64_t AssignStreams (int64_t stream);

protected:
  virtual void DoInitialize (void);
private:

  void SendXBroadcast(void);
  void SendRBroadcast(void);
  void SendWBroadcast(void);
  void RecvShide (Ptr<Socket> socket);
  void RecvPureBroadcast();
  void RecvWindingBroadcast();
  void RecvWinding2Broadcast();
  void RecvWinding3Broadcast();
  void GetSourcePosition();
  void GetRsuPosition();
  void GetDestPosition();
  void Get_mountain_rsu_list();
  void NewSourceDirection();
  void RecvTime();
  //void DelayTime();
  //std::map< std::string,int> positoinXmap;
  //std::map<int,double> positoinXmap{};
  /// IP protocol
  Ptr<Ipv4> m_ipv4;
  /// Nodes IP address
  Ipv4Address m_mainAddress;
  /// Raw unicast socket per each IP interface, map socket -> iface address (IP + mask)
  ///各IPインタフェースごとの未処理のユニキャストソケット、マップソケット - > ifaceアドレス（IP +マスク）
  std::map< Ptr<Socket>, Ipv4InterfaceAddress > m_socketAddresses;
  /// Raw subnet directed broadcast socket per each IP interface, map socket -> iface address (IP + mask)
  ///各IPインタフェースごとに未処理のサブネット指向のブロードキャストソケット、マップソケット -  ifaceアドレス（IP +マスク）
  std::map< Ptr<Socket>, Ipv4InterfaceAddress > m_socketSubnetBroadcastAddresses;
  /// Loopback device used to defer RREQ until packet will be fully formed
  /// パケットが完全に形成されるまでRREQを延期するために使用されるループバックデバイス
  Ptr<NetDevice> m_lo;
  
 
 
private:
 
  /// Provides uniform random variables. 均一な確率変数を提供します。
  Ptr<UniformRandomVariable> m_uniformRandomVariable;

};

} //namespace shide
} //namespace ns3

#endif /* SHIDEROUTINGPROTOCOL_H */
